import React, { Component } from "react";
import '../Bureau.css'

class CarnetEntretienP extends Component {
    render(){
        return(
            <div className="margin_auto">
                <div className="rectangle f_dir_col">
                    <div className="display_row margin_auto">
                       <p className="txt_color_grey">Vous voici dans la partie la plus consultée de votre locataire. <br/>
                           En remplissant tous les champs demandés, vous actionnerez l’assistant chargé <br/>
                           de rappeler une action à faire à la personne concernée : vous ou votre locataire. <br/>
                           Certaines de ses actions vous seront rapportées, afin que vous suiviez le bon usage
                           de votre bien.
                       </p>
                    </div>
                    <div className="display_row space_between">
                        <div className="petitrectangle">Les installations et équipements</div>
                        <div className="petitrectangle">Relevés des compteurs</div>
                    </div>
                </div>
            </div>
        )
    }
}
export default CarnetEntretienP ;
