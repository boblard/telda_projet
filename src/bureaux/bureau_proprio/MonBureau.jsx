import React, { Component } from "react";
import '../Bureau.css'
import HomePage from '../../homepage/HomePage';

class MonBureau extends Component {

    render(){
        return(
          <div class="margin_auto">
            <div class="profile_container">
              <div class="profile_ring"></div>
              <p class="profile_title">Bienvenue</p>
            </div>
            <div className="rectangle f_dir_col">
              <div className="display_row margin_auto">
                  <div onClick={this.props.onClick} name="biens" className="petitrectangle btn1 ">Mes Biens</div>
                  <div onClick={this.props.onClick} name="quifaitquoi" className="petitrectangle btn2 margin_left_62px">Qui fait quoi ?</div>
                  <div onClick={this.props.onClick} name="agenda" className="petitrectangle btn3 margin_left_62px">Votre agenda</div>
              </div>
              <div className="display_row margin_top_54px margin_auto ">
                  <div onClick={this.props.onClick} name="urgences" className="petitrectangle btn4">Procédures d'urgence</div>
                  <div onClick={this.props.onClick} name="5" className="petitrectangle btn5 margin_left_62px">Votre service bientôt disponible ici</div>
                  <div onClick={this.props.onClick} name="savoirplus" className="petitrectangle btn6 margin_left_62px">En savoir plus</div>
              </div>
            </div>
          </div>
        )
    }
}
export default MonBureau;
