import React, {Component} from "react";
import logotelda from "../../images/logotelda.png";
import "../Bureau.css";
import clochenonotification32px from "../../images/clochenonotification32px.svg";

import MonBureau from "./MonBureau";
import SyntheseDesComptes from "./SyntheseDesComptes";
import MenuMesBiens from "./MenuMesBiens";
import QuiFaitQuoi from "./QuiFaitQuoi";
import ProceduresUrgence from "./ProceduresUrgence";
import Agenda from "./Agenda";
import Parametres from "./Parametres";
import EnSavoirPlus from "./EnSavoirPlus";
import Contact from "./Contact";
import AccueilDuBien from "./AccueilDuBien";
import CarteIdentiteAppartement from "./CarteIdentiteAppartement";
import NumerosUtilesP from "./NumerosUtilesP";
import LoyersDepenses from "./LoyersDepenses";
import CarnetEntretienP from "./CarnetEntretienP";
import MonLocataire from "./MonLocataire";
import VieQuotidienneP from "./VieQuotidienneP";
import NoticeEntretienP from "./NoticeEntretienP";


class Bureau extends Component {

    state = {
        currentPage: undefined,
        active: ''
    };

    handleClick = event => {
        this.setState({currentPage: event.currentTarget.getAttribute('name')})
        this.setState({active: event.currentTarget.getAttribute('id')});
    };

    render() {
        var currentpage;

        if (this.state.currentPage === undefined) {
            currentpage = <MonBureau onClick={ this.handleClick }/>;
        } else if (this.state.currentPage === "bureau") {
            currentpage = <MonBureau onClick={ this.handleClick }/>;
        } else if (this.state.currentPage === "synthese") {
            currentpage = <SyntheseDesComptes/>;
        } else if (this.state.currentPage === "biens") {
            currentpage = <MenuMesBiens onClick={ this.handleClick }/>;
        } else if (this.state.currentPage === "quifaitquoi") {
            currentpage = <QuiFaitQuoi/>;
        } else if (this.state.currentPage === "urgences") {
            currentpage = <ProceduresUrgence/>;
        } else if (this.state.currentPage === "agenda") {
            currentpage = <Agenda/>;
        } else if (this.state.currentPage === "parametres") {
            currentpage = <Parametres/>;
        } else if (this.state.currentPage === "savoirplus") {
            currentpage = <EnSavoirPlus/>;
        } else if (this.state.currentPage === "contact") {
            currentpage = <Contact/>;
        } else if (this.state.currentPage === "accueildubien") {
            currentpage = <AccueilDuBien onClick={ this.handleClick }/>
        } else if (this.state.currentPage ==="carteidentiteappartement") {
            currentpage = <CarteIdentiteAppartement onClick={ this.handleClick }/>
        } else if (this.state.currentPage === "numerosutiles") {
            currentpage = <NumerosUtilesP onClick={ this.handleClick }/>
        } else if (this.state.currentPage === "loyerdepenses") {
            currentpage = <LoyersDepenses onClick={ this.handleClick }/>
        } else if (this.state.currentPage === "carnetentretien") {
            currentpage = <CarnetEntretienP onClick={ this.handleClick }/>
        } else if (this.state.currentPage ==="monlocataire") {
            currentpage = <MonLocataire onClick={ this.handleClick }/>
        } else if (this.state.currentPage === "viequotidienne") {
            currentpage = <VieQuotidienneP onClick={ this.handleClick } />
        }else if (this.state.currentPage === "noticeentretien") {
            currentpage = <NoticeEntretienP onClick={ this.handleClick } />
        }
        else {alert("Il y a une erreur, veuillez recharger la page.")}

        return(
            <div className="max">
                <div className="">
                    <nav className="bcnd_blue flex f_spacebetween h_nav fixed_nav">
                        <div className="center_parent p_5px">
                            <img className="center_enfant p_5px margin-t15px margin-l10vh"
                                            src={logotelda} alt="imagelogo"/>
                        </div>
                        <div className="flex f_dir_row">
                            {/*<div className="center_parent"><input className="search_nav p_5px" type="search" placeholder="Recherche..."/></div>*/}
                            <div className="carre100px"><img className="notif" alt="notif" src={clochenonotification32px}/></div>
                        </div>
                    </nav>
                    <div className="flex f_spacebetween fixed_aside_left">
                        <aside className="container_left flex f_dir_col f_spacebetween sidenav sidenavP">

                            <div className={this.state.active === "1" || this.state.currentPage === undefined ? 'active': ''}>
                                <p id="1" onClick={this.handleClick} name="bureau">Mon bureau</p>
                            </div>
                            <div className={this.state.active === "2"? 'active': ''}>
                                <p id="2" onClick={this.handleClick} name="synthese">Synthèse des comptes</p>
                            </div>
                            <div className={this.state.active === "3"? 'active': ''}>
                                <p id="3" onClick={this.handleClick} name="biens">Mes Biens </p>
                            </div>
                            <div className={this.state.active === "4"? 'active': ''}>
                                <p id="4" onClick={this.handleClick} name="quifaitquoi">Qui fait quoi ?</p>
                            </div>
                            <div className={this.state.active === "5"? 'active': ''}>
                                <p id="5" onClick={this.handleClick} name="urgences">Procédures d'urgence</p>
                            </div>
                            <div className={this.state.active === "6"? 'active': ''}>
                                <p id="6" onClick={this.handleClick} name="agenda">Votre agenda</p>
                            </div>
                            <div className={this.state.active === "7"? 'active': ''}>
                                <p id="7" onClick={this.handleClick} name="parametres">Paramètres</p>
                            </div>
                            <div className={this.state.active === "8"? 'active': ''}>
                                <p id="8" onClick={this.handleClick} name="savoirplus">En savoir plus</p>
                            </div>
                            <div className={this.state.active === "9"? 'active': ''}>
                                <p id="9" onClick={this.handleClick} name="contact">Nous contacter</p>
                            </div>
                            <div>
                                <p className="logout">Se déconnecter</p>
                            </div>
                        </aside>
                        <aside className="container_right">
                            {currentpage}
                        </aside>
                    </div>
                </div>
            </div>
        )
    }
}
export default Bureau;
