import React, { Component } from "react";
import '../Bureau.css'

class VieQuotidienneP extends Component {
    render(){
        return(
            <div className="width_100p bcnd_grey">
                <div>
                    <div className="container_white text_align_center margin_auto">
                        <p className="padding_t10px txt_color_blue">CODE D'ACCES</p>
                    </div>
                    <div className="width_40vw text_align_center margin_auto">
                        <div className="f_dir_row space_between">
                            <p>Interphone : </p>
                            <input name="interphone" type="text" placeholder="numéro"/>
                            <p>Porte d'entrée : </p>
                            <input name="porteentree1" type="text" placeholder="porte d'entrée"/>
                        </div>
                        <div className="f_dir_row space_between padding_t10px">
                            <p>Ascenseur : </p>
                            <input name="ascenseur" type="text" placeholder="ascenseur"/>
                            <p>Porte d'entrée 2 : </p>
                            <input name="porteentree2" type="text" placeholder="porte d'entrée 2"/>
                        </div>
                        <div className="f_dir_row space_between padding_t10px">
                            <p>Clé WIFI : </p>
                            <input className="width_75pcent" type="text" placeholder="12345 12345 12345 12345"/>
                        </div>
                    </div>
                    <div className="container_white text_align_center margin_auto">
                        <p className="padding_t10px txt_color_blue">PASSAGE DU COURRIER</p>
                    </div>
                    <div className="width_40vw text_align_center margin_auto">
                        <div className="f_dir_row space_between">
                            <p>En semaine : </p>
                            <input name="courriersemaine" type="text" placeholder="10h00"/>
                            <p>Le samedi : </p>
                            <input name="courriersamedi" type="text" placeholder="13h00"/>
                        </div>
                    </div>
                    <div className="container_white text_align_center margin_auto">
                        <p className="padding_t10px txt_color_blue">GESTION DES DECHETS</p>
                    </div>
                    <div className="width_75pcent margin_auto">
                        <p>La distribution des sacs poubelles se fait en fonction du nombre
                            d’habitants dans le logement. </p>
                        <p>Votre locataire doit se faire connaitre auprès de ce distributeur : </p>
                        <div className="f_dir_row ">
                            <input type="text" placeholder="Nom de la société"/>
                            <p>Téléphone : </p>
                            <input name="telsacpoubelle" type="number" placeholder="0123456789"/>
                        </div>
                        <h2 className="txt_color_blue">Les déchets recyclables</h2>
                        <div className="f_dir_row space_between">
                            <select name="typedechets">
                                <option disabled selected value> -- Type de déchets --</option>
                                <option value="verres">Verres</option>
                                <option value="plastique">Plastiques</option>
                                <option value="conserve">Boites de conserves</option>
                                <option value="conserve">Boites de conserves</option>
                                <option value="papiercarton">Papier et carton ( non gras)</option>
                                <option value="vegetaux">Végétaux</option>
                            </select>
                            <select className="margin-l5px" name="typepoubelle" id="typepoubelle">
                                <option disabled selected value> -- Type de poubelle --</option>
                                <option value="sac">Sac poubelle</option>
                                <option value="cagette">Cagette</option>
                                <option value="poubelle">Poubelle</option>
                                <option value="conteneur">Conteneur</option>
                            </select>
                            <select className="margin-l5px" name="couleurpoubelle" id="couleurpoubelle">
                                <option disabled selected value> -- Choisir une couleur --</option>
                                <option value="verte">Verte</option>
                                <option value="jaune">Jaune</option>
                                <option value="marron">Marron</option>
                                <option value="bleu">Bleu</option>
                                <option value="rose">Rose</option>
                                <option value="gris">Gris</option>
                            </select>
                            <select className="margin-l5px" name="jourramassage" id="jourramassage"
                                    placeholder="jour de ramaasage">
                                <option disabled selected value> -- Choisir un jour --</option>
                                <option value="lundi">Lundi</option>
                                <option value="mardi">Mardi</option>
                                <option value="mercredi">Mercredi</option>
                                <option value="jeudi">Jeudi</option>
                                <option value="vendredi">Vendredi</option>
                                <option value="samedi">Samedi</option>
                            </select>
                        </div>
                        <div className="text_align_center">
                            <button type="submit" className="">Confirmer</button>
                        </div>
                        <h2 className="txt_color_blue">Les déchets non recyclables</h2>
                        <div className="f_dir_row space_between inline_block">
                            <select name="typepoubellenonrecyclable" id="typepoubelleonrecyclable">
                                <option disabled selected value> -- Type de poubelle --</option>
                                <option value="sac">Sac poubelle</option>
                                <option value="cagette">Cagette</option>
                                <option value="poubelle">Poubelle</option>
                                <option value="conteneur">Conteneur</option>
                            </select>
                            <select className="margin-l5px" name="couleurpoubelle" id="couleurpoubelle">
                                <option disabled selected value> -- Choisir une couleur --</option>
                                <option value="verte">Verte</option>
                                <option value="jaune">Jaune</option>
                                <option value="marron">Marron</option>
                                <option value="bleu">Bleu</option>
                                <option value="rose">Rose</option>
                                <option value="gris">Gris</option>
                            </select>
                            <select className="margin-l5px" name="jourramassage" id="jourramassage"
                                    placeholder="jour de ramaasage">
                                <option disabled selected value> -- Choisir un jour --</option>
                                <option value="lundi">Lundi</option>
                                <option value="mardi">Mardi</option>
                                <option value="mercredi">Mercredi</option>
                                <option value="jeudi">Jeudi</option>
                                <option value="vendredi">Vendredi</option>
                                <option value="samedi">Samedi</option>
                            </select>
                        </div>
                        <div className="text_align_center">
                            <button type="submit" className="">Confirmer</button>
                        </div>
                        <h2 className="txt_color_blue">Relève des encombrants</h2>
                        <p>Sauf huiles de vidange, produit toxique et pharmaceutique. Voir la réglementation de
                            ramassage de votre ville</p>
                        <div className="f_dir_row space_between">
                            <select name="encombrants">
                                <option disabled selected value> -- Faire un choix --</option>
                                <option value="1erjourencombrants">Les 1ers</option>
                                <option value="2ejourencombrants">Les 2èmes</option>
                                <option value="3ejourencombrants">Les 3èmes</option>
                                <option value="4ejourencombrants">Les 4èmes</option>
                            </select>
                            <select className="margin-l5px" name="jourencombrants">
                                <option disabled selected value> -- Selectionner un jour --</option>
                                <option value="lundiencombrants">Lundi</option>
                                <option value="mardiencombrants">Mardi</option>
                                <option value="mercrediencombrants">Mercredi</option>
                                <option value="jeudiencombrants">Jeudi</option>
                                <option value="vendrediencombrants">Vendredi</option>
                                <option value="samediencombrants">Samedi</option>
                            </select>
                            <select className="margin-l5px" name="frequenceencombrants">
                                <option disabled selected value> -- Selectionner une fréquence --</option>
                                <option value="mensuel">Mensuel</option>
                                <option value="bimensuel">Bimestriel</option>
                                <option value="trimestriel">Trimestriel</option>
                                <option value="semestriel">Semestriel</option>
                                <option value="annuel">Annuel</option>
                            </select>
                        </div>
                        <div className="text_align_center">
                            <button type="submit" className="">Confirmer</button>
                        </div>
                        <h2 className="txt_color_blue">Adresse de la déchetterie</h2>

                        <div className="text_align_center ">
                            <input className="margin-tb5" type="text" placeholder="Complément d'informations"/>
                        </div>
                        <div className="text_align_center">
                            <input className="margin-tb5" type="text" placeholder="N°"/>
                            <input className="margin-tb5" type="text" placeholder="Rue / Voie / Chemin"/>
                        </div>
                        <div className="text_align_center">
                            <input className="margin-tb5" type="text" placeholder="Code postal"/>
                            <input className="margin-tb5" type="text" placeholder="Ville"/>
                        </div>

                        <h2 className="txt_color_blue">Horaires de la déchetterie</h2>
                        <div className="width_75pcent text_align_center space_between">
                            <p className="inline_block">Semaine : </p>
                            <p className="inline_block">Samedi : </p>
                        </div>
                        <div className="width_75pcent text_align_center space_between">
                            <p className="inline_block">Dimanche : </p>
                            <p className="inline_block">Jours fériés : </p>
                        </div>
                    </div>
                    <div className="container_white text_align_center margin_auto">
                        <p className="padding_t10px txt_color_blue">PLANS DES TRANSPORTS URBAINS</p>
                    </div>
                    <div className="width_75pcent margin_auto">
                        <p>A l’attention de vos locataires, choisissez le plan qui correspond à votre ville,
                            et copiez/collez le lien internet correspondant à chaque catégories ci-dessous</p>
                        <div className="f_dir_row text_align_center">
                            <p>Bus :</p>
                            <input className="txt_color_blue" type="file" name="bus"/>
                            <p>Métro :</p>
                            <input className="txt_color_blue" type="file" name="metro"/>
                            <p>Tramway :</p>
                            <input className="txt_color_blue" type="file" name="tramway"/>
                            <p>Vélib'</p>
                            <input className="txt_color_blue" type="file" name="velib"/>
                        </div>
                    </div>
                    <div className="container_white text_align_center margin_auto">
                        <p className="padding_t10px txt_color_blue">USAGE ET FONCTIONNEMENT DU QUARTIER</p>
                    </div>
                    <div className="width_75pcent margin_auto text_align_center">
                        <p>Adresses utiles :
                            <select className="margin_left_62px" name="adressesutiles">
                                <option disabled selected value> -- Faire un choix --</option>
                                <option value="mairire">Mairie</option>
                                <option value="hoteldeville">Hôtel de ville</option>
                                <option value="laposte">Bureau de poste</option>
                                <option value="police">Commissariat</option>
                            </select>
                        </p>
                        <div className="f_spacearound margin-tb5">
                            <input className="padding_t10px" type="text" placeholder="Numéro"/>
                            <input className="padding_t10px margin-l5px" name="rue" type="text"
                                   placeholder="Rue / Avenue"/>
                        </div>
                        <div className="f_spacearound margin-tb5">
                            <input type="text" placeholder="Codepostal"/>
                            <input className="margin-l5px" type="text" placeholder="Ville"/>
                        </div>
                        <div className="text_align_center inline_block f_spacearound margin-tb5">
                            <p>Téléphone de l'accueil :
                                <input className="margin-l5px" type="text" placeholder="Tél"/>
                            </p>
                        </div>
                        <div className="text_align_center margin-tb5">
                            <input className="" name="email" type="text" placeholder="Email"/>
                        </div>
                    </div>
                </div>
                <div className="space_between">
                    <div className="text_align_left">
                        <button type="submit" className="">Retour au menu</button>
                    </div>
                    <div className="text_align_right">
                        <button type="submit" className="">Confirmer</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default VieQuotidienneP ;
