import React, { Component } from "react";
import '../Bureau.css';
import appartement from "../../images/appartement.svg";
import lot from "../../images/lot.svg";
import home from "../../images/home.svg"


class MenuMesBiens extends Component {
  render() {
    return(

        <div className="width_100p padding_mesBiens">
          <div className="flex f_dir_row f_spacebetween padding_mesBiens">
            <div className="padding_t20px margin_auto">
              <img className="icone" src={home} alt="icone"/>
                <label className="padding_l20px inline_block">MAISON(S)</label>
            </div>
            <div className="padding_t10px margin_auto">
              <img className="icone" src={appartement} alt="icone"/>
                <label className="padding_l20px inline_block">APPARTEMENT(S)</label>
              </div>
              <div className="padding_t20px margin_auto">
                <img className="icone" src={lot} alt="icone"/>
                  <label className="padding_l20px inline_block">LOT(S) <br/> D'HABITATION</label>
              </div>
            </div>
            <div className="rectangle f_dir_col">
              <div className="display_row margin_auto">
              <div className="petitrectangle_mesbiens">Synthese des comptes</div>
              <div onClick={this.props.onClick} name="accueildubien"className="petitrectangle_mesbiens margin_left_62px">8 rue de lille 59100 Roubaix</div>
              <div className="petitrectangle_mesbiens margin_left_62px">Synthese des comptes</div>
            </div>
            <div className="display_row margin_top_54px margin_auto">
              <div className="petitrectangle_mesbiens">Synthese des comptes</div>
              <div className="petitrectangle_mesbiens margin_left_62px">Synthese des comptes</div>
              <div className="petitrectangle_mesbiens margin_left_62px">Synthese des comptes</div>
            </div>
          </div>
          <div className="width_auto text_align_center">
            <button type="submit" id="btn_mesbiens">ENREGISTER UN NOUVEAU BIEN</button>
          </div>
        </div>
    );
  }
}

export default MenuMesBiens;
