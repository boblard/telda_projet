import React, { Component } from "react";
import '../Bureau.css'

class MonLocataire extends Component {
    render(){
        return(
            <div className="width_100p bcnd_grey">
                   <div className="champ_obligatoire">
                       <p>* Merci de remplir les champs</p>
                   </div>
                    <div className="container_white text_align_center margin_auto">
                        <p className="padding_t10px">LE BAIL</p>
                    </div>
                <div className="w-quatrevingtpcent margin_auto">
                        <p>Renseignez les champs, tels que définis dans le Bail :</p>
                        <label>Date d’effet du Bail : </label>
                    <input type="date" id="dateBail" className="" placeholder="JJ/MM/AAAA"/>
                    <p for="choixtrimestre">Trimestre de référence :
                        <select id="choixtrimestre" className="width_auto f_spacearound">
                            <option value="">--Veuillez faire un choix--</option>
                            <option value="trimestre1">1er trimestre</option>
                            <option value="trimestre2">2eme trimestre</option>
                            <option value="trimestre3">3eme trimestre</option>
                            <option value="trimestre4">4eme trimestre</option>
                        </select>
                    </p>
                    <p>(Pour le calcul de la révision annuelle du loyer. S'il n'a pas été stipulé,
                        prendre le dernier indice connu à la date de signature). </p>
                    </div>
            </div>
        )
    }
}
export default MonLocataire ;
