import React, {Component} from "react";
import '../Bureau.css'

class AccueilDuBien extends Component {
    render(){
        return(
            <div className="margin_auto">
                <div className="rectangle f_dir_col">
                    <div className="display_row margin_auto">
                        <div onClick={this.props.onClick} name="loyerdepenses"
                             className="petitrectangle btn2">Tableau de bord <br/>loyers et dépenses
                        </div>
                        <div onClick={this.props.onClick} name="carnetentretien"
                             className="petitrectangle btn2 margin_left_62px">Carnet d'entretien
                        </div>
                        <div onClick={this.props.onClick} name="monlocataire"
                             className="petitrectangle btn2 margin_left_62px">Mon locataire
                        </div>
                    </div>
                    <div className="display_row margin_top_54px margin_auto ">
                        <div onClick={this.props.onClick} name="carteidentiteappartement"
                             className="petitrectangle btn2">Carte d'identité
                        </div>
                        <div onClick={this.props.onClick} name="viequotidienne"
                             className="petitrectangle btn2 margin_left_62px">Vie quotidienne
                        </div>
                        <div onClick={this.props.onClick} name="numerosutiles"
                             className="petitrectangle btn2 margin_left_62px">Numéros utiles
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}
export default AccueilDuBien ;
