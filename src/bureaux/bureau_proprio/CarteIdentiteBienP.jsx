import React, {Component} from "react";
import '../Bureau.css';
import '../../tunnel_achat/index.css';
import arrow from '../../images/arrow.png';

class CarteIdentiteBienP extends Component {
    render(){
        return(
            <div className="width_100p bcnd_grey">
              <div className="champ_obligatoire">
                <p>* Merci de remplir les champs</p>
              </div>



                <div className="padding_l20pour">
                  <div className="titre_proprio">
                    <p>DESTINATION DE LA LOCATION :</p>
                  </div>
                  <div className="display_padd">
                    <div className="boxsizing ">
                      <input type="checkbox"
                         id="bailH"
                         name="bail"
                         value="Bail d'Habitation"required></input>
                       <label htmlFor="bailH">Bail d'Habitation</label>
                    </div>
                    <div className="boxsizing">
                      <input type="checkbox"
                          id="meuble"
                          name="meuble"
                          value="meublé"required></input>
                        <label htmlFor="meuble">Meublé *</label>
                    </div>
                   </div>
                   <div className="display_padd1">
                     <div className="boxsizing">
                       <input type="checkbox"
                          id="bailC"
                          name="bail"
                          value="Bail Commercial"required></input>
                        <label htmlFor="bailC">Bail Commercial</label>
                      </div>
                      <div className="boxsizing">
                        <input type="checkbox"
                           id="Nmeuble"
                           name="meuble"
                           value="Non meublé"required></input>
                         <label htmlFor="Nmeuble">Non Meublé *</label>
                       </div>
                    </div>
                    <div className="flex f_dir_row">
                      <p className="padding_menuentretien"><span className="underline">Date de construction</span> *</p>
                      <input type="text"
                        id="annee"
                        name="annee"
                        placeholder="AAAA"
                        className="margin_top_4demip"
                        />
                    </div>

                    <div className="titre_proprio margin_top_4demip">
                      <p>TYPE ET DESCRIPTIF :</p>
                    </div>
                    <div>
                      <p>F3 Comprenant</p>
                    </div>
                    <div className="flex f_dir_row margin_left30p">
                      <p >Surface Totale Habitable Louée</p>
                      <input type="text"
                        id="surfaceHabitable"
                        name="surfaceHabitable"
                        className="margin_top_2p"/>
                      <p className="padding_l3p reinitialise"> m²</p>
                    </div>

                    <div className="titre_proprio margin_top_4demip">
                      <p>DOCUMENT A IMPORTER :</p>
                    </div>
                    <p>Concernant la maison </p>
                      <div className="flex f_spacebetween">
                        <button type="submit" id="regle">Règlement Intérieur<img src={arrow} alt="Flèche descendante" id="img_arrow"/></button>
                        <button type="submit" id="regle">Règlement Intérieur<img src={arrow} alt="Flèche descendante" id="img_arrow"/></button>
                      </div>
                      <div className="flex f_spacebetween margin_top_4demip">
                        <div className="f_dir_col f_spacebetween">
                          <p>Assurance *</p>
                          <button type="submit" id="regle">Règlement Intérieur<img src={arrow} alt="Flèche descendante" id="img_arrow"/></button>
                        </div>

                        <div className="f_dir_col f_spacebetween margin_right30px">
                          <div className="flex f_spacebetween">
                            <p>Date d'échéance</p>
                            <input type="text"
                            id="dateEcheance"
                            name="dateEcheance"
                            placeholder="jj/mm/aaaa"/>
                          </div>
                          <div className="flex f_spacebetween">
                            <p>N° de contrat</p>
                            <input type="text"
                            id="n°Contrat"
                            name="n°Contrat"
                            placeholder="123456789654321"/>
                          </div>
                          <div className="flex f_spacebetween">
                            <p>Nom de l'assureurs</p>
                            <input type="text"
                            id="nomAssureur"
                            name="nomAssureur"
                            placeholder="Exemple Assurance"/>
                          </div>
                          <div className="flex f_spacebetween">
                            <p>Téléphone</p>
                            <input type="text"
                            id="telephone"
                            name="telephone"
                            placeholder="06..."/>
                          </div>
                        </div>
                      </div>
                      <p>Diagnostiques *</p>
                        <div className="flex f_spacebetween">

                          <button type="submit" id="regle">DPE (Performances Energétiques) <img src={arrow} alt="Flèche descendante" id="img_arrow"/></button>
                          <button type="submit" id="regle">Risques naturels<img src={arrow} alt="Flèche descendante" id="img_arrow"/></button>
                        </div>
                      <p>Sécurité</p>
                        <div className="flex f_spacebetween">
                          <button type="submit" id="regle">Détecteur de fumées<img src={arrow} alt="Flèche descendante" id="img_arrow"/></button>
                        </div>
                      </div>
                 </div>

        )
    }
}

export default CarteIdentiteBienP;
