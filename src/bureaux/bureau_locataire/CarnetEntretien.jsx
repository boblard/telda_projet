import React, { Component } from "react";
import '../Bureau.css'

class CarnetEntretien extends Component {
    render(){
        return(
            <div className="margin_auto">
                <div className="rectangle f_dir_col">
                    <div className="display_row margin_auto">
                        <div onClick={this.props.onClick} name="noticeentretien"
                             className="petitrectangle btn1 vert1 ">Notice d'entretien
                        </div>
                        <div onClick={this.props.onClick} name="factureentretien"
                             className="petitrectangle btn2 margin_left_62px vert2">Facture d'entretien
                        </div>
                        <div onClick={this.props.onClick} name="relevescompteurs"
                             className="petitrectangle btn2 margin_left_62px vert3">Relevé des compteurs
                        </div>

                    </div>
                    <div className="display_row margin_top_54px margin_auto ">
                        <div onClick={this.props.onClick} name="carteidentitelogement"
                             className="petitrectangle btn1 vert3">Carte d'identité du logement
                        </div>
                        <div onClick={this.props.onClick} name="modeemploi"
                             className="petitrectangle btn5 margin_left_62px vert4">Mode d'emploi
                        </div>
                        <div onClick={this.props.onClick} name="numerosutiles"
                             className="petitrectangle btn5 margin_left_62px vert1">Numéros utiles
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default CarnetEntretien ;
