import React, { Component } from "react";
import logotelda from "../../images/logotelda.png";
import "../Bureau.css";
import clochenonotification32px from "../../images/clochenonotification32px.svg";
import MonBureau from "./MonBureau";
import AgendaL from "./AgendaL";
import QuiFaitQuoiL from "./QuiFaitQuoiL";
import ProceduresUrgenceL from "./ProceduresUrgenceL";
import ParametresL from "./ParametresL";
import EnSavoirPlusL from "./EnSavoirPlusL";
import ContactL from "./ContactL";
import CarnetEntretien from './CarnetEntretien';
import VieQuotidienne from './VieQuotidienne';
import DossierLocation from './DossierLocation';
import NoticeEntretien from './NoticeEntretien';
import FactureEntretien from './FactureEntretien';
import RelevesCompteurs from './RelevesCompteurs';
import CarteIdentiteLogement from './CarteIdentiteLogement';
import ModeEmploi from './ModeEmploi';
import NumerosUtiles from './NumerosUtiles';


class BureauL extends Component {

  state = {
    currentPage: undefined,
    active: ''
  };


    handleClick = event => {
        this.setState({currentPage: event.currentTarget.getAttribute('name')})
        this.setState({active: event.currentTarget.getAttribute('id')});
    }

    render(){
      console.log(this.state);
        var currentpage;

        if (this.state.currentPage === undefined){
            currentpage = <MonBureau onClick={this.handleClick} />;
        } if (this.state.currentPage === "bureauL") {
            currentpage = <MonBureau onClick={this.handleClick} />;
        } if (this.state.currentPage === "agendaL") {
            currentpage = <AgendaL />;
        } if (this.state.currentPage === "quifaitquoiL") {
            currentpage = <QuiFaitQuoiL />;
        } if (this.state.currentPage === "urgenceL") {
            currentpage = <ProceduresUrgenceL />;
        } if (this.state.currentPage === "parametresL") {
            currentpage = <ParametresL />;
        } if (this.state.currentPage === "savoirplusL") {
            currentpage = <EnSavoirPlusL />;
        } if (this.state.currentPage === "contactL"){
            currentpage = <ContactL />;
        } if (this.state.currentPage === "carnetentretien") {
            currentpage = <CarnetEntretien onClick={this.handleClick} />;
        } if (this.state.currentPage === "viequotidienne") {
            currentpage = <VieQuotidienne />;
        } if (this.state.currentPage === "dossierlocation") {
            currentpage = <DossierLocation />;
        } if (this.state.currentPage === "noticeentretien") {
            currentpage = <NoticeEntretien />;
        } if (this.state.currentPage === "factureentretien") {
            currentpage = <FactureEntretien />;
        } if (this.state.currentPage === "relevescompteurs") {
            currentpage = <RelevesCompteurs />;
        } if (this.state.currentPage === "carteidentitelogement") {
            currentpage = <CarteIdentiteLogement />;
        } if (this.state.currentPage === "modeemploi") {
            currentpage = <ModeEmploi />;
        } if (this.state.currentPage === "numerosutiles") {
            currentpage = <NumerosUtiles />;
        }


        return(
            <div className="max">
            <div className="">

                <nav className="bcnd_green flex f_spacebetween h_nav">
                    <div className="center_parent p_5px">
                            <img
                                className="center_enfant p_5px margin-t15px margin-l10vh"
                                src={logotelda}
                                alt="logo"
                            />
                    </div>
                    <div className="flex f_dir_row ">
                        <div className="center_parent">
                            {/*<input*/}
                            {/*    className="search_nav p_5px"*/}
                            {/*    type="search"*/}
                            {/*    placeholder="Recherche..."*/}
                            {/*/>*/}
                        </div>
                        <div className="carre100px">
                            <img
                                className="notif"
                                src={clochenonotification32px}
                                alt="notif"
                            />
                        </div>
                    </div>
                </nav>
                <div className="flex f_spacebetween">
                <aside className="container_left flex f_dir_col f_spacebetween sidenav sidenavL">
                    <div className={this.state.active === "1" || this.state.currentPage === undefined ? 'active': ''}>
                        <p id="1"  onClick={this.handleClick} name="bureauL">
                            Mon bureau
                        </p>
                    </div>
                    <div className={this.state.active === "2"? 'active': ''}>
                        <p id="2" onClick={this.handleClick} name="agendaL">
                            Mon Agenda
                        </p>
                    </div>
                    <div className={this.state.active === "3"? 'active': ''}>
                        <p id="3" onClick={this.handleClick} name="quifaitquoiL">
                            Qui fait quoi ?
                        </p>
                    </div>
                    <div className={this.state.active === "4"? 'active': ''}>
                        <p id="4" onClick={this.handleClick} name="urgenceL">
                            Procédures d'urgence
                        </p>
                    </div>
                    <div className={this.state.active === "5"? 'active': ''}>
                        <p id="5" onClick={this.handleClick} name="parametresL">
                            Paramètres
                        </p>
                    </div>
                    <div className={this.state.active === "6"? 'active': ''}>
                        <p id="6" onClick={this.handleClick} name="savoirplusL">
                            En savoir plus
                        </p>
                    </div>
                    <div className={this.state.active === "7"? 'active': ''}>
                        <p id="7" onClick={this.handleClick} name="contactL">
                            Nous contacter
                        </p>
                    </div>
                    <div>
                        <p  name="" className="logout" >
                            Se déconnecter
                        </p>
                    </div>
                </aside>
                <aside className="container_right flex">
                    {currentpage}
                </aside>
            </div>
            </div>
            </div>
        )
    }
};

export default BureauL;
