import React, { Component } from "react";

import '../Bureau.css'
import HomePage from '../../homepage/HomePage';

class MonBureau extends Component {

    render(){
        return(
            <div class="margin_auto width_75pcent">
                <div class="profile_container">
                    <div class="profile_ring"></div>
                    <p class="profile_title">Bienvenue</p>
                </div>
                <div className="rectangle f_dir_col">
                    <div className="display_row margin_auto">
                        <div onClick={this.props.onClick} name="carnetentretien" className="petitrectangle btn1 vert1 ">Carnet d'entretien</div>
                        <div onClick={this.props.onClick} name="viequotidienne"  className="petitrectangle btn2 margin_left_62px vert2">Vie Quotidienne</div>

                    </div>
                    <div className="display_row margin_top_54px margin_auto ">
                        <div onClick={this.props.onClick} name="dossierlocation" className="petitrectangle btn4 vert3">Mon dossier location</div>
                        <div onClick={this.props.onClick} name="agendaL" className="petitrectangle btn5 margin_left_62px vert4">Mon Agenda</div>
                    </div>
                </div>
            </div>
        )
    }
}
export default MonBureau;
