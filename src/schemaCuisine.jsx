<!DOCTYPE html/>
<html lang="fr">

	<head>
		<title>SCHEMA CUISINE</title>
		<meta charset="utf-8"/>
		<link rel="stylesheet" type="text/css" href="../../telda/Telda-Pages/schemas-html/schema-cuisine-style.css"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
		<meta name="description" content="API" />
		<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>

	<script>
$(document).ready(function(){
$("#chauffe-eau").click(function(){
$(".chauffe-eau-info-bulle").toggleClass("chauffe-eau-info-bulle-visibility");});
$(".cross").click(function(){
$(".chauffe-eau-1-info-bulle-visibility").toggleClass(".chauffe-eau-info-bulle");
});
});

$(document).ready(function(){
$("#thermostat").click(function(){
$(".thermostat-info-bulle").toggleClass("thermostat-info-bulle-visibility");});
$(".cross").click(function(){
$(".thermostat-info-bulle-visibility").toggleClass(".thermostat-info-bulle");
});
});

$(document).ready(function(){
$("#rat").click(function(){
$(".rat-info-bulle").toggleClass("rat-info-bulle-visibility");});
$(".cross").click(function(){
$(".rat-info-bulle-visibility").toggleClass(".rat-info-bulle");
});
});

$(document).ready(function(){
$("#carrelage").click(function(){
$(".carrelage-info-bulle").toggleClass("carrelage-info-bulle-visibility");});
$(".cross").click(function(){
$(".carrelage-info-bulle-visibility").toggleClass(".carrelage-info-bulle");
});
});

$(document).ready(function(){
$("#souris").click(function(){
$(".souris-info-bulle").toggleClass("souris-info-bulle-visibility");});
$(".cross").click(function(){
$(".souris-info-bulle-visibility").toggleClass(".souris-info-bulle");
});
});

$(document).ready(function(){
$("#robinet-evier").click(function(){
$(".robinet-evier-info-bulle").toggleClass("robinet-evier-info-bulle-visibility");});
$(".cross").click(function(){
$(".robinet-evier-info-bulle-visibility").toggleClass(".robinet-evier-info-bulle");
});
});
</script>
	<body>

	<div class="schema">

	<div id="chauffe-eau"><!--1--><div class="chauffe-eau-info-bulle"><div class="titre-et-cross"><div class="id-bulle"><p><b>Chauffe-eau</b></p></div><div class="id-cross"><img src="../../telda/Telda-Pages/schemas-html/img/272-cross.svg" class="cross"></div></div><div class="descriptif"><p>Lorem ipsum dolor sit amet</p></div></div></div>
	<div id="thermostat"><!--1--><div class="thermostat-info-bulle"><div class="titre-et-cross"><div class="id-bulle"><p><b>Thermostat</b></p></div><div class="id-cross"><img src="../../telda/Telda-Pages/schemas-html/img/272-cross.svg" class="cross"></div></div><div class="descriptif"><p>Lorem ipsum dolor sit amet</p></div></div></div>
	<div id="rat"><!--1--><div class="rat-info-bulle"><div class="titre-et-cross"><div class="id-bulle"><p><b>Rat</b></p></div><div class="id-cross"><img src="../../telda/Telda-Pages/schemas-html/img/272-cross.svg" class="cross"></div></div><div class="descriptif"><p>Lorem ipsum dolor sit amet</p></div></div></div>
	<div id="carrelage"><!--1--><div class="carrelage-info-bulle"><div class="titre-et-cross"><div class="id-bulle"><p><b>Carrelage</b></p></div><div class="id-cross"><img src="../../telda/Telda-Pages/schemas-html/img/272-cross.svg" class="cross"></div></div><div class="descriptif"><p>Lorem ipsum dolor sit amet</p></div></div></div>
	<div id="souris"><!--1--><div class="souris-info-bulle"><div class="titre-et-cross"><div class="id-bulle"><p><b>Souris</b></p></div><div class="id-cross"><img src="../../telda/Telda-Pages/schemas-html/img/272-cross.svg" class="cross"></div></div><div class="descriptif"><p>Lorem ipsum dolor sit amet</p></div></div></div>
	<div id="robinet-evier"><!--1--><div class="robinet-evier-info-bulle"><div class="titre-et-cross"><div class="id-bulle"><p><b>Robinet-evier</b></p></div><div class="id-cross"><img src="../../telda/Telda-Pages/schemas-html/img/272-cross.svg" class="cross"></div></div><div class="descriptif"><p>Lorem ipsum dolor sit amet</p></div></div></div>

	</div>
	</body>
	</html>
