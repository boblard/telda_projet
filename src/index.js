import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from 'react-router-dom';

import HomePage from './homepage/HomePage';
import Inscription from './tunnel_achat/Inscription';
import Bureau from './bureaux/bureau_proprio/Bureau';
import BureauL from './bureaux/bureau_locataire/Bureau';
import SyntheseDesComptes from './bureaux/bureau_proprio/SyntheseDesComptes';
import LoginPage from "./homepage/LoginPage";
import QuiFaitQuoi from "./bureaux/bureau_proprio/QuiFaitQuoi";
import ProceduresUrgence from "./bureaux/bureau_proprio/ProceduresUrgence";
import Agenda from "./bureaux/bureau_proprio/Agenda";
import MesBiens from "./bureaux/bureau_proprio/MenuMesBiens";
import EnSavoirPlus from "./bureaux/bureau_proprio/EnSavoirPlus"
import Parametres from "./bureaux/bureau_proprio/Parametres"
import Contact from "./bureaux/bureau_proprio/Contact"
import CarnetEntretien from "./bureaux/bureau_locataire/CarnetEntretien";
import DossierLocation from "./bureaux/bureau_locataire/DossierLocation";
import LoyersCharges from "./bureaux/bureau_locataire/LoyersCharges";
import VieQuotidienne from "./bureaux/bureau_locataire/VieQuotidienne";
import QuiFaitQuoiL from "./bureaux/bureau_locataire/QuiFaitQuoiL"
import EnSavoirPlusL from "./bureaux/bureau_locataire/EnSavoirPlusL"
import ContactL from "./bureaux/bureau_locataire/ContactL"
import ProceduresUrgenceL from "./bureaux/bureau_locataire/ProceduresUrgenceL";
import ParametresL from "./bureaux/bureau_locataire/ParametresL";
import AgendaL from "./bureaux/bureau_locataire/AgendaL";
import TestBDD from "./TestBDD";
import TestPostApi from "./TestPostApi";



class App extends Component {
  render() {
    return (
      <BrowserRouter>
              <div>
                <div className="main-route-place">
                  <Route exact path="/" component={HomePage} />
                  <Route exact path="/homepage/LoginPage" component={LoginPage} />
                  <Route exact path="/inscription" component={Inscription} />
                  <Route exact path="/bureau/propriétaire" component={Bureau} />
                  <Route exact path="/bureau/propriétaire/SyntheseDesComptes" component={SyntheseDesComptes} />
                  <Route exact path="/bureau/propriétaire/MesBiens" component={MesBiens} />
                  <Route exact path="/bureau/propriétaire/QuiFaitQuoi" component={QuiFaitQuoi} />
                  <Route exact path="/bureau/propriétaire/ProdeduresUrgence" component={ProceduresUrgence} />
                  <Route exact path="/bureau/propriétaire/EnSavoirPlus" component={EnSavoirPlus} />
                  <Route exact path="/bureau/propriétaire/Agenda" component={Agenda} />
                  <Route exact path="/bureau/propriétaire/Parametres" component={Parametres} />
                  <Route exact path="/bureau/propriétaire/Contact" component={Contact} />
                  <Route exact path="/bureau/locataire" component={BureauL} />
                  <Route exact path="/bureau/locataire/CarnetEntretien" component={CarnetEntretien} />
                  <Route exact path="/bureau/locataire/ContactL" component={ContactL} />
                  <Route exact path="/bureau/locataire/DossierLocation" component={DossierLocation} />
                  <Route exact path="/bureau/locataire/EnSavoirPlusL" component={EnSavoirPlusL} />
                  <Route exact path="/bureau/locataire/LoyersCharges" component={LoyersCharges} />
                  <Route exact path="/bureau/locataire/ParametresL" component={ParametresL} />
                  <Route exact path="/bureau/locataire/ProceduresUrgenceL" component={ProceduresUrgenceL} />
                  <Route exact path="/bureau/locataire/QuiFaitQuoiL" component={QuiFaitQuoiL} />
                  <Route exact path="/bureau/locataire/VieQuotidienne" component={VieQuotidienne} />
                  <Route exact path="/bureau/locataire/AgendaL" component={AgendaL} />
                  <Route exact path="/test" component={TestBDD} />
                </div>
              </div>
      </BrowserRouter>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
