import React, { Component } from "react";
import "../tunnel_achat/index.css";
import HomePage from "./HomePage";


class LoginPage extends Component {
    render(){
        return(
            <div className="container_form_left">
                <HomePage />
            <form>
            <div className="displayflex_center margin-tb5 display_column">
               Votre identifiant : <input className="w-vingtpcent" name="mailLogin" type="email" placeholder="Email"></input>
            <div className="displayflex_center margin-tb5 display_column">
                Votre mot de passe : <input className="w-vingtpcent" name="mdpLogin" type="password" placeholder="Mot de passe"></input>
            </div>
            </div>
                <button className="container_button_validation width_40vw" type="submit">Se connecter</button>
            </form>
            </div>
        )
    }
}
export default LoginPage;
