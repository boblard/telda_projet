import React, {Component} from "react";
import {Redirect} from 'react-router-dom';
import "../bureaux/Bureau.css";

class HomePage extends Component {

  state = {
    redirect: false
  };
  setRedirect = event => {
    this.setState({
      redirect: true,
      redirectionRoute: event.currentTarget.getAttribute("route")
    })
  };
  renderRedirect = () => {
    if (this.state.redirect) {
      console.log(this.state.redirectionRoute);
       return <Redirect to={this.state.redirectionRoute} />
    }
  };

  render(){
    return(
        <div className="max">
        <nav className="bcnd_blue h_nav">
            <div className="flex f_dir_row text_align_center jusitfy_content">
          {this.renderRedirect()}

          <button className="" onClick={this.setRedirect} route="/inscription" type="button">Inscription</button>
          <button onClick={this.setRedirect} route="/homepage/LoginPage" type="button">Connexion</button>
          <button onClick={this.setRedirect} route="/bureau/propriétaire" type="button">Je suis propriétaire </button>
          <button onClick={this.setRedirect} route="/bureau/locataire" type="button">Je suis locataire</button>
          <a href="http://www.mytelda.fr/"><button>Landing Page </button></a>
            </div>
      </nav>
            <div className="">
            </div>
        </div>
    )
  }
}
export default HomePage;
