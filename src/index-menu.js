import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import { syncHistoryWithStore } from 'react-router-redux';

import './tunnel_achat/index.css'

import HomePage from './homepage/HomePage';
import Inscription from './tunnel_achat/Inscription';
import Bureau from './bureaux/bureau_proprio/Bureau';
import BureauP from './bureaux/bureau_locataire/Bureau'

import ClientForm from './tunnel_achat/ClientForm';
import HeaderPage from './tunnel_achat/HeaderPage';
import LoginPage from "./homepage/LoginPage";


class App extends Component {
  render() {
    return (
      <BrowserRouter>
              <div>
                <ul>
                  <li>
                    <Link to="/">HomePage</Link>
                  </li>
                  <li>
                    <Link to="/inscription">Inscription</Link>
                  </li>
                  <li>
                    <Link to="/bureau/propriétaire">Bureau proprio</Link>
                  </li>
                  <li>
                    <Link to="/bureau/locataire">Bureau locataire</Link>
                  </li>
                  <li>
                    <Link to="/homepage/LoginPage">Page de connexion</Link>
                  </li>
                </ul>
                <hr />
                <div className="main-route-place">
                  <Route exact path="/" component={HomePage} />
                  <Route exact path="/inscription" component={Inscription} />
                  <Route exact path="/bureau/propriétaire" component={Bureau} />
                  <Route exact path="/bureau/locataire" component={BureauP} />
                  <Route exact path="/homepage/LoginPage" component={LoginPage} />
                </div>
              </div>
      </BrowserRouter>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
