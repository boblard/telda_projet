import React, { Component } from "react";


class FormRepresentantS extends Component {

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }


  render(){
    return(
      <div>
          <div className="display_row margin-b20">
              <div className="display_column">
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="sci2"
                          name="type_associe_rep_soc"
                          onChange={this.handleChange}
                          value="sci">
                      </input>
                      <label htmlFor="sci2">SCI</label>
                  </div>
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="sarl2"
                          name="type_associe_rep_soc"
                          onChange={this.handleChange}
                          value="sarl">
                      </input>
                      <label htmlFor="sarl2">SARL familiale</label>
                  </div>
              </div>
              <div className="display_column">
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="scpi2"
                          name="type_associe_rep_soc"
                          onChange={this.handleChange}
                          value="scpi">
                      </input>
                      <label htmlFor="scpi2">SCPI</label>
                  </div>
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="autre3"
                          name="type_associe_rep_soc"
                          onChange={this.handleChange}
                          value="autre">
                      </input>
                      <label htmlFor="autre3">Autre</label>
                  </div>
              </div>
          </div>

              <div className="boxsizing">
                      <input
                          className="input_100 boxsizing"
                          name="denomination_associe_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Dénomination">
                      </input>
              </div>
              <div className="boxsizing">
                      <input
                          className="input_100 boxsizing"
                          name="nomsoc_associe_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Nom de la Société">
                      </input>
              </div>
              <p className="blue_color padding_5px">Adresse postale de la société</p>
              <div className="">
                      <input
                          className="boxsizing w-centpcent"
                          name="residence_associe_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Nom de la résidence">
                      </input>
              </div>
              <div className="space_between">
                  <input className="w-quarantecinqpcent"
                         name="batiment_associe_rep_soc"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Bâtiment">
                  </input>
                  <input className="w-quarantecinqpcent"
                         name="entree_associe_rep_soc"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Entrée">
                  </input>
              </div>
              <div className="space_between">
                  <input className="w-quarantecinqpcent"
                         name="appart_associe_rep_soc"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Appartement">
                  </input>
                  <input className="w-quarantecinqpcent"
                         name="etage_associe_rep_soc"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Etage">
                  </input>
              </div>
              <div className="">
                  <div className="space_between">
                      <input
                          className="boxsizing"
                          name="boite_postale_associe_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="BP ou CS ">
                      </input>
                  </div>
                  <div className="space_between">
                      <input className="w-quarantecinqpcent"
                             name="code_postal_associe_rep_soc"
                             onChange={this.handleChange}
                             type="text"
                             placeholder="Code postal">
                      </input>
                      <input className="w-quarantecinqpcent"
                             name="ville_associe_rep_soc"
                             onChange={this.handleChange}
                             type="text"
                             placeholder="Ville">
                      </input>
                  </div>
                  <div className="space_between">
                      <input
                          className="boxsizing"
                          name="pays_associe_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Pays">
                      </input>
                  </div>
              </div>
          <p className="blue_color padding_5px">Représentée par :</p>

                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="associe_soc_male"
                          name="civilite_associe_rep_soc"
                          onChange={this.handleChange}
                          value="monsieur">
                      </input>
                      <label htmlFor="associe_soc_male">Monsieur</label>
                  </div>
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="associe_soc_female"
                          name="civilite_associe_rep_soc"
                          onChange={this.handleChange}
                          value="madame">
                      </input>
                      <label htmlFor="associe_soc_female">Madame</label>
                  </div>
                  <div className="boxsizing">
                      <input
                          className="input_100 boxsizing"
                          name="nom_associe_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Nom">
                      </input>
                  </div>
                  <div className="boxsizing">
                      <input
                          className="input_100 boxsizing"
                          name="prenom_associe_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Prénom">
                      </input>
                  </div>
                   <div className="space_between">
                       <input className="w-quarantecinqpcent"
                             name="ddn_associe_rep_soc"
                             onChange={this.handleChange}
                             type="date"
                             placeholder="Date de naissance">
                      </input>
                       <input className="w-quarantecinqpcent"
                             name="vdn_associe_rep_soc"
                             onChange={this.handleChange}
                             type="text"
                             placeholder="Lieu de naissance">
                      </input>
                   </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent"
                               name="telephone_associe_rep_soc"
                               onChange={this.handleChange}
                               type="text"
                               placeholder="N° téléphone">
                        </input>
                        <input className="w-quarantecinqpcent"
                               name="email_associe_rep_soc"
                               onChange={this.handleChange}
                               type="text"
                               placeholder="Email">
                        </input>
                    </div>
        </div>
    )
  }
}
export default FormRepresentantS;
