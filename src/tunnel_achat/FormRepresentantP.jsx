import React, { Component } from "react";


class FormRepresentantP extends Component {

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }


  render(){
    return(
      <div>
        <div className="boxsizing">
            <input
                type="radio"
                id="civilite_ste2"
                name="civilite_ste_rep_part"
                onChange={this.handleChange}
                value="monsieur">
            </input>
                <label htmlFor="civilite_ste2">Monsieur</label>
        </div>
        <div className="boxsizing">
            <input
                type="radio"
                id="civilite_ste"
                name="civilite_ste_rep_part"
                onChange={this.handleChange}
                value="madame">
            </input>
                <label htmlFor="civilite_ste">Madame</label>
        </div>
        <div className="boxsizing">
            <input
                className="input_100 boxsizing"
                name="nom_ste_rep_part"
                onChange={this.handleChange}
                type="text"
                placeholder="Nom" required>
            </input>
        </div>
        <div className="boxsizing">
            <input
                className="input_100 boxsizing"
                name="prenom_ste_rep_part"
                onChange={this.handleChange}
                type="text"
                placeholder="Prénom" required>
            </input>
        </div>
        <div className="space_between">
            <input className="w-quarantecinqpcent"
                   name="ddn_ste_rep_part"
                   onChange={this.handleChange}
                   type="date"
                   placeholder="Date de naissance">
            </input>
            <input className="w-quarantecinqpcent"
                   name="vdn_ste_rep_part"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Lieu de naissance">
            </input>
        </div>
        <div className="space_between">
            <input className="w-quarantecinqpcent"
                   name="telephone_ste_rep_part"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="N° téléphone">
            </input>
            <input className="w-quarantecinqpcent"
                   name="email_ste_rep_part"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Email" required>
            </input>
        </div>
        <div className="boxsizing">
            <p className="blue_color padding_5px">Votre adresse postale</p>
        </div>
        <div className="">
            <input
                className="boxsizing w-centpcent"
                name="residence_ste_rep_part"
                onChange={this.handleChange}
                type="text"
                placeholder="Nom de la résidence">
            </input>
        </div>
        <div className="space_between">
          <input className="w-quarantecinqpcent"
                 name="batiment_ste_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Bâtiment">
          </input>
          <input className="w-quarantecinqpcent"
                 name="entree_ste_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Entrée">
          </input>
        </div>
        <div className="space_between">
          <input className="w-quarantecinqpcent"
                 name="appart_ste_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Appartement">
          </input>
          <input className="w-quarantecinqpcent"
                 name="etage_ste_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Etage">
          </input>
        </div>
          <div className=" space_between">
              <input className="boxsizing" name="numero" onChange={this.handleChange} type="text" placeholder="N°" required></input>
              <input className="boxsizing w-quatrevingtdixpcent" name="rue" onChange={this.handleChange} type="text" placeholder="NOM DE LA RUE / VOIE / CHEMIN" required></input>
          </div>
        <div className="space_between">
          <input className="w-quarantecinqpcent"
                 name="code_postal_ste_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Code postal" required>
          </input>
          <input className="w-quarantecinqpcent"
                 name="ville_ste_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Ville" required>
          </input>
        </div>
        <div className="">
            <input
                className="boxsizing"
                name="pays_ste_rep_part"
                onChange={this.handleChange}
                type="text"
                placeholder="Pays" required>
            </input>
        </div>
      </div>
    )
  }
}
export default FormRepresentantP;
