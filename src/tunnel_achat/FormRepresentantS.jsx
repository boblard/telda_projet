import React, { Component } from "react";


class FormRepresentantS extends Component {

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }


  render(){
    return(
      <div>
          <div className="display_row margin-b20">
              <div className="display_column">
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="sci2"
                          name="type_ste_rep_soc"
                          onChange={this.handleChange}
                          value="sci">
                      </input>
                      <label htmlFor="sci2">SCI</label>
                  </div>
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="sarl2"
                          name="type_ste_rep_soc"
                          onChange={this.handleChange}
                          value="sarl">
                      </input>
                      <label htmlFor="sarl2">SARL familiale</label>
                  </div>
              </div>
              <div className="display_column">
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="scpi2"
                          name="type_ste_rep_soc"
                          onChange={this.handleChange}
                          value="scpi">
                      </input>
                      <label htmlFor="scpi2">SCPI</label>
                  </div>
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="autre3"
                          name="type_ste_rep_soc"
                          onChange={this.handleChange}
                          value="autre3">
                      </input>
                      <label htmlFor="autre3">Autre</label>
                  </div>
              </div>
          </div>

              <div className="boxsizing">
                      <input
                          className="input_100 boxsizing"
                          name="denomination_ste_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Dénomination">
                      </input>
              </div>
              <div className="boxsizing">
                      <input
                          className="input_100 boxsizing"
                          name="nom_ste_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Nom de la Société">
                      </input>
              </div>
              <p className="blue_color padding_5px">Adresse postale de la société</p>
              <div className="">
                      <input
                          className="boxsizing w-centpcent"
                          name="residence_ste_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Nom de la résidence">
                      </input>
              </div>
              <div className="space_between">
                  <input className="w-quarantecinqpcent"
                         name="batiment_ste_rep_soc"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Bâtiment">
                  </input>
                  <input className="w-quarantecinqpcent"
                         name="entree_ste_rep_soc"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Entrée">
                  </input>
              </div>
              <div className="space_between">
                  <input className="w-quarantecinqpcent"
                         name="appart_ste_rep_soc"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Appartement">
                  </input>
                  <input className="w-quarantecinqpcent"
                         name="etage_ste_rep_soc"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Etage">
                  </input>
              </div>
              <div className="">
                  <div className="space_between">
                      <input
                          className="boxsizing"
                          name="boite_postale_ste_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="BP ou CS ">
                      </input>
                  </div>
                  <div className="space_between">
                      <input className="w-quarantecinqpcent"
                             name="code_postal_ste_rep_soc"
                             onChange={this.handleChange}
                             type="text"
                             placeholder="Code postal">
                      </input>
                      <input className="w-quarantecinqpcent"
                             name="ville_ste_rep_soc"
                             onChange={this.handleChange}
                             type="text"
                             placeholder="Ville">
                      </input>
                  </div>
                  <div className="space_between">
                      <input
                          className="boxsizing"
                          name="pays_ste_rep_soc"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Pays">
                      </input>
                  </div>
              </div>
          <p className="blue_color padding_5px">Représentée par :</p>

                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="civilite_representant_ste"
                          name="civilite_ste_rep_soc_rep"
                          onChange={this.handleChange}
                          value="monsieur">
                      </input>
                      <label htmlFor="civilite_representant_ste">Monsieur</label>
                  </div>
                  <div className="boxsizing">
                      <input
                          type="radio"
                          id="civilite_representant_ste2"
                          name="civilite_ste_rep_soc_rep"
                          onChange={this.handleChange}
                          value="madame">
                      </input>
                      <label htmlFor="civilite_representant_ste2">Madame</label>
                  </div>
                  <div className="boxsizing">
                      <input
                          className="input_100 boxsizing"
                          name="nom_ste_rep_soc_rep"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Nom">
                      </input>
                  </div>
                  <div className="boxsizing">
                      <input
                          className="input_100 boxsizing"
                          name="prenom_ste_rep_soc_rep"
                          onChange={this.handleChange}
                          type="text"
                          placeholder="Prénom">
                      </input>
                  </div>
                   <div className="space_between">
                       <input className="w-quarantecinqpcent"
                              name="ddn_ste_rep_soc_rep"
                              onChange={this.handleChange}
                              type="date"
                              placeholder="Date de naissance">
                      </input>
                       <input className="w-quarantecinqpcent"
                              name="vdn_ste_rep_soc_rep"
                              onChange={this.handleChange}
                              type="text"
                              placeholder="Lieu de naissance">
                      </input>
                   </div>
          <div className="space_between">
              <input className="w-quarantecinqpcent"
                     name="telephone_ste_rep_soc_rep"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="N° téléphone">
              </input>
              <input className="w-quarantecinqpcent"
                     name="email_ste_rep_soc_rep"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Email">
              </input>
          </div>
        </div>
    )
  }
}
export default FormRepresentantS;
