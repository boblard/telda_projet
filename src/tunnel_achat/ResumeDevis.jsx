import React, { Component } from "react";
import * as moment from 'moment';

class ResumeDevis extends Component {

  state = {};

    render() {

      // test variable pr devis //
      var CiviliteClient,
    NomClient,
    PrenomClient,
    NomRes,
    BatimentClient,
    EntreeClient,
    AppartClient,
    EtageClient,
    ComplAdre,
    VilleClient,
    NumeroClient,
    RueClient,
    DenomiationSociete,
    TypeSociete,
    NomSociete,
    ComplAdreSociete,
    BatimentSociete,
    EntreeSociete,
    AppartSociete,
    EtageSociete,
    BPSociete,
    CpSociete,
    VilleSociete,
    CpClient,
    CiviliteRepPart,
    NomRepPart,
    PrenomRepPart,
    NomRepSociete,
    NomResBien,
    BatimentBien,
    EntreeBien,
    AppartBien,
    EtageBien,
    ComplAdreBien,
    NumeroBien,
    RueBien,
    CpBien,
    VilleBien,
    TypeBien,
    result,
    tva,
    ht,
    NatureLogement,
    CodePromo;
      var id = this.props.id;

      console.log("etat de formdevis");
      console.log(this.state);

      //var objets = Object.assign(objet1, objet2);



       var tva = (result * 20 / 100);
       var ht = result - tva;

      if (this.props.prevState.forms[1] === undefined || this.props.prevState.forms[2] === undefined || this.props.prevState.forms[3] === undefined) {
        CiviliteClient = "";
        NomClient = "";
        PrenomClient = "";
        NomRes = "";
        BatimentClient = "";
        EntreeClient = "";
        AppartClient = "";
        EtageClient = "";
        ComplAdre = "";
        VilleClient = "";
        NumeroClient = "";
        RueClient = "";
        CpClient = "";
        DenomiationSociete = "";
        TypeSociete = "";
        NomSociete = "";
        ComplAdreSociete = "";
        BatimentSociete = "";
        EntreeSociete = "";
        AppartSociete = "";
        EtageSociete = "";
        BPSociete = "";
        CpSociete = "";
        VilleSociete ="";
        NomRepPart ="";
        PrenomRepPart = "";
        CiviliteRepPart = "";
        NomRepSociete = "";
        NomResBien = "";
        BatimentBien = "";
        EntreeBien ="";
        AppartBien = "";
        EtageBien = "";
        ComplAdreBien = "";
        NumeroBien = "";
        TypeBien = "";
        RueBien = "";
        CpBien = "";
        VilleBien = "";
        NatureLogement = "";
        result = 100;
        tva = "";
        ht = "";
      }

       if (this.props.id >= 1){
           var datas = this.props.prevState.forms[1].form_object

         CiviliteClient = datas.civilite;
         NomClient = datas.nom;
         PrenomClient = datas.prenom;
         ComplAdre = datas.complement;
         EntreeClient = datas.entree;
         EtageClient = datas.etage;
         BatimentClient = datas.batiment;
         AppartClient = datas.appart;
         NumeroClient = datas.numero;
         RueClient = datas.rue;
         VilleClient = datas.ville;
         CpClient = datas.code_postal;
         DenomiationSociete = datas.denomination;
         TypeSociete = datas.type_societe;
         NomSociete = datas.nom_societe;
         ComplAdreSociete = datas.complement_societe;
         BatimentSociete = datas.batiment_societe;
         EntreeSociete = datas.entree_societe;
         AppartSociete = datas.appart_societe;
         EtageSociete = datas.etage_societe;
         BPSociete = datas.bp_societe;
         CpSociete = datas.code_postal_societe;
         VilleSociete = datas.ville_societe;
         NomRepPart = datas.nom_ste_rep_part;
         PrenomRepPart = datas.prenom_ste_rep_part;
         CiviliteRepPart = datas.civilite_ste_rep_part;
         NomRepSociete = datas.nom_ste_rep_soc;
       }

       if  (this.props.id >= 2){
         var datas = this.props.prevState.forms[2].form_object;
         console.log("datas");
         NomResBien = datas.residence_bien;
         BatimentBien = datas.batiment_bien;
         EntreeBien = datas.entree_bien;
         AppartBien = datas.appart_bien;
         EtageBien = datas.etage_bien;
         ComplAdreBien = datas.complement_bien;
         NumeroBien = datas.numero_bien;
         TypeBien = datas.type_habitation;
         RueBien = datas.rue_bien;
         CpBien = datas.code_postal_bien;
         VilleBien = datas.ville_bien;
       }

       if (this.props.id >= 3){

         var datas =  this.props.prevState.forms[3].form_object;

         NatureLogement = datas.taille_bien;
       }

      //   // fonction date du jour, date echeance //
      //
          var DateJour = moment.utc().format('DD/MM/YYYY');
          var DateEch = moment().subtract(1, "days" ,).add(1,"years").format("DD/MM/YYYY");
      //
       // generer random num facture apres date ///
        function randomNumber() {
                var random;
                random = Math.floor(Math.random() * 10);
                return random;
        };
        var numAlea = randomNumber();
        var NumFact = moment.utc().format('YYYYMMDD') + numAlea;

         console.log("devis");
         console.log(this.props.prevState.forms[1]);
         console.log("data");
         console.log(this.props.prevState.forms[2]);
         console.log("prevstate 1");
         console.log(this.props.prevState.forms[1]);
         console.log("prevstate 2");
         console.log(this.props.prevState.forms[2]);
         console.log("prevstate 3");
         console.log(this.props.prevState.forms[3]);
        // console.log(datas.id);

        return(
            <div className="w-centpcent">
                <div className="adresse padding_bt5lr30">
                    <p className="margin-tb5">telda</p>
                    <p className="margin-tb5">rue du fontenoy</p>
                    <p className="margin-tb5">59100 Roubaix</p>
                </div>
                <div className="adresseClient padding_bt5lr30">
                    <p className="margin-tb5"><span>{ CiviliteClient } { DenomiationSociete }</span> <span>{ PrenomClient} { TypeSociete }</span> <span id="nomclient">{ NomClient } { NomSociete }</span>  </p>
                    <p className="margin-tb5"><span>{ CiviliteRepPart} { NomRepPart } { PrenomRepPart } { NomRepSociete }</span></p>
                    <p className="margin-tb5">{ ComplAdre } { BatimentClient } { EntreeClient } { EtageClient} { AppartClient } { ComplAdreSociete } { BatimentSociete } { EntreeSociete } { EtageSociete} { AppartSociete } { BPSociete }</p>
                    <p className="margin-tb5"><span>{ NumeroClient} { RueClient }</span></p>
                    <p className="margin-tb5" id="villeclient">{ CpClient } { VilleClient } { CpSociete } { VilleSociete }</p>
                </div>
                <div className="adresseClient padding_bt5lr30">
                    <div className="margin-tb5">Roubaix, le { DateJour }</div>
                </div>
                <div className="padding_bt5lr30">
                    <div className="cadreFacture padding_bt5lr30 display_row">
                       <p>Facture Contrat n° { NumFact } </p>
                    </div>
                </div>
                <div className="">
                    <div className="adresseBien padding_bt5lr30 display_row">
                        <p className="pGauche">concerne le bien :</p>
                        <p className="pDroite">{ NomResBien } { BatimentBien } { EntreeBien } { AppartBien } { EtageBien } { ComplAdreBien } { NumeroBien } { RueBien } { CpBien } { VilleBien }</p>
                    </div>
                    <div className="typeBien padding_bt5lr30 display_row">
                       <p className="pGauche">type du bien :</p>
                        <p className="pDroite"> { TypeBien }</p>
                    </div>

                    <div className="natureLogement padding_bt5lr30 display_row">
                        <p className="pGauche">nature du logement :</p>
                        <p className="pDroite"> { NatureLogement }</p>
                    </div>
                    <div className="montantDevis padding_bt5lr30 display_row">
                        <p className="pGauche">Le montant du devis s'élève à : </p>
                        <p className="pDroite"> { result }  €  TTC <br/> { tva } € TVA<br/> { ht } € HT </p>
                    </div>
                    <div className="priseEffet padding_bt5lr30 display_row">
                        <p className="pGauche">Date de prise d'effet :</p>
                        <p className="pDroite"> { DateJour }  </p>
                    </div>
                    <div className="dateEcheance padding_bt5lr30 display_row">
                        <p className="pGauche">Date d'écheance : </p>
                        <p className="pDroite"> { DateEch }  </p>
                    </div>
                    <div className="renouvellement padding_bt5lr30">
                        <p className=""><span className="souligne">renouvellement :</span> à l'issue de la première année de gestion de votre bien et dans le délai légal de résiliation, vous recevrez un mandat de prélèvement SEPA avec la possibilité d'opter pour un réglement annuel ou mensuel ( 1/12 du montant de la facture contrat, sans frais de gestion).</p>
                        <p><span className="souligne">Attention :</span> votre tarif est révisé à la date d'échéance de ce contrat sur la base du dernier indice publié par SYNTEC</p>
                        <p className="">Notre prestation comprend l'accès aux outils de gestion locatives suivantes : carnet d'entretien du bien, documentation technique et personnalisée</p>
                    </div>
                </div>
            </div>
        );
    }
}
export default ResumeDevis;
