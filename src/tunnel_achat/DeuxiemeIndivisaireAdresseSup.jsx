import React, { Component } from "react";
import DeuxiemeParticulierAdresseSup from "./DeuxiemeParticulierAdresseSup";

class DeuxiemeIndivisaireAdresseSup extends Component {

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }

    render() {
        return (
            <div>
                <div className="">
                    <input className="boxsizing w-centpcent"
                           name="deux_residence"
                           onChange={this.handleChange}
                           type="text"
                           placeholder="Nom de la résidence">
                    </input>
                </div>
                <div className="">
                    <input className="boxsizing w-centpcent"
                           name="deux_infos_batiment"
                           onChange={this.handleChange}
                           type="text"
                           placeholder="N° Bâtiment - Entrée - Appart - Étage">
                    </input>
                </div>
                <div className="">
                    <input className="boxsizing w-centpcent" n
                           ame="deux_complement"
                           onChange={this.handleChange}
                           type="text"
                           placeholder="Complément d’adresse">
                    </input>
                </div>
                <div className=" space_between">
                    <input className="boxsizing"
                           name="deux_numero"
                           onChange={this.handleChange}
                           type="text"
                           placeholder="N°">
                    </input>
                    <input className="boxsizing w-quatrevingtdixpcent"
                           name="deux_rue"
                           onChange={this.handleChange}
                           type="text"
                           placeholder="NOM DE LA RUE / VOIE / CHEMIN">
                    </input>
                </div>
                <div className="space_between">
                    <input className="boxsizing"
                           name="deux_code_postal"
                           onChange={this.handleChange}
                           type="text"
                           placeholder="Code postal">
                    </input>
                    <input className="boxsizing w-quatrevingtpcent"
                           name="deux_ville"
                           onChange={this.handleChange}
                           type="text"
                           placeholder="Ville">
                    </input>
                </div>
                <div className="">
                    <input className="boxsizing"
                           name="deux_pays"
                           onChange={this.handleChange}
                           type="text"
                           placeholder="deux_Pays">
                    </input>
                </div>
                <div className="space_between">
                    <input className="boxsizing"
                           name="deux_telephone"
                           onChange={this.handleChange}
                           type="number"
                           placeholder="Numéro de téléphone">
                    </input>
                </div>
            </div>
        )
    }
}


export default DeuxiemeParticulierAdresseSup;
