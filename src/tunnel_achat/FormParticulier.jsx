import React, {Component} from "react";

import DeuxiemeParticulier from "./DeuxiemeParticulier";
class FormParticulier extends Component {

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }

  render() {
    // probleme vu que le state est vide et qu'il se rempli lorsqu'on clique sur ajout, les state ne sont pas presents
    //donc pas d'acces au state deuxProprio
    if (this.props.prevState.pays === "test"){window.scrollTo(0,0)};
    var proprioSup;
    var textButton = "Ajouter un 2ème propriétaire";
      if(this.props.prevState.deuxProprio == true){
        proprioSup = <DeuxiemeParticulier prevState={this.props.prevState} onClickAdresseDif={this.props.onClickAdresseDif} onClick={this.props.onClick} onChange={this.props.onChange}/>;
       textButton = "Annuler";
       }else {
         proprioSup = undefined;
       }

    return(
                  <form onSubmit={this.props.onSubmit}>
                    <div className="padding_l20p">
                      <div className="container_input_local">
                          <div className="boxsizing ">
                              <input type="radio"
                                     id="choice4"
                                     onChange={this.handleChange}
                                     name="civilite"
                                     value="madame"required></input>
                            <label htmlFor="choice4">Madame</label>
                          </div>
                          <div className="boxsizing margin-b20 margin-t10px">
                              <input type="radio"
                                     id="choice5"
                                     onChange={this.handleChange}
                                     name="civilite"
                                     value="monsieur"></input>
                              <label htmlFor="choice5">Monsieur</label>
                          </div>
                          <div className="container_form_form1 _3">

                              <div className="">
                                  <input className="input_100 boxsizing" name="nom" onChange={this.handleChange} type="text" placeholder="NOM"required></input>
                              </div>
                              <div className="">
                                  <input className="input_100 boxsizing" name="prenom" onChange={this.handleChange} type="text" placeholder="Prénom"required></input>
                              </div>
                              <div className="space_between">
                                  <input className="w-quarantecinqpcent" name="ddn" onChange={this.handleChange} type="date" placeholder="Date de naissance"></input>
                                  <input className="w-quarantecinqpcent" name="vdn" onChange={this.handleChange} type="text" placeholder="Lieu de naissance"></input>
                              </div>
                              <div className="space_between">
                                  <input className="w-quarantecinqpcent" name="n°_telephone" onChange={this.handleChange} type="text" placeholder="N° téléphone"required></input>
                                  <input className="w-quarantecinqpcent" name="email" onChange={this.handleChange} type="text" placeholder="Email" required></input>
                              </div>
                              <p className="blue_color padding_5px">Votre adresse postale</p>
                              <div className="">
                                <input className="boxsizing w-centpcent" name="residence" onChange={this.handleChange} type="text" placeholder="Nom de la résidence"></input>
                              </div>
                              <div className="space_between">
                                <input className="w-quarantecinqpcent" name="batiment" onChange={this.handleChange} type="text" placeholder="Bâtiment"></input>
                                <input className="w-quarantecinqpcent" name="entree" onChange={this.handleChange} type="text" placeholder="Entrée"></input>
                              </div>
                              <div className="space_between">
                                <input className="w-quarantecinqpcent" name="appart" onChange={this.handleChange} type="text" placeholder="Appartement"></input>
                                <input className="w-quarantecinqpcent" name="etage" onChange={this.handleChange} type="text" placeholder="Etage"></input>
                              </div>
                              <div className="">
                                <input className="boxsizing w-centpcent" name="complement" onChange={this.handleChange} type="text" placeholder="Complément d’adresse"></input>
                              </div>
                              <div className=" space_between">
                                  <input className="boxsizing" name="numero" onChange={this.handleChange} type="text" placeholder="N°" required></input>
                                  <input className="boxsizing w-quatrevingtdixpcent" name="rue" onChange={this.handleChange} type="text" placeholder="NOM DE LA RUE / VOIE / CHEMIN" required></input>
                              </div>
                              <div className="space_between">
                                  <input className="w-quarantecinqpcent" name="code_postal" onChange={this.handleChange} type="text" placeholder="Code postal" required></input>
                                  <input className="w-quarantecinqpcent" name="ville" onChange={this.handleChange} type="text" placeholder="Ville" required></input>
                              </div>
                              <div className="space_between">
                                <input className="w-quarantecinqpcent" name="pays" onChange={this.handleChange} type="text" placeholder="Pays"required></input>
                              </div>

                          </div>
                      </div>
                      <div className="container_input_local">

                        <div className="container_button displayflex_center">
                          <button type="button" id="new_prop" onClick={this.props.onClickDeuxProprio}>{textButton}</button>
                        </div>
                            {proprioSup}
                        <div className="container_button displayflex_center">
                          <button type="submit">CONTINUER</button>
                        </div>
                      </div>
                  </div>
                </form>
    );
  }
}
export default FormParticulier;
