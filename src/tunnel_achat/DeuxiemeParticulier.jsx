import React, { Component } from "react";

import DeuxiemeParticulierAdresseSup from "./DeuxiemeParticulierAdresseSup"

class DeuxiemeParticulier extends Component {

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }

  render(){

    var adresseSup;

     if(this.props.prevState.adresse_dif === true){
       adresseSup = <DeuxiemeParticulierAdresseSup prevState={this.props.prevState} onChange={this.props.onChange} />;
       } else {
         adresseSup = undefined;
       };
    return(
      <div className="margin-t10px">

            <div className="boxsizing">
                <input type="radio"
                       id="choice1_deux"
                       onChange={this.handleChange}
                       name="deux_civilite"
                       value="madame">
                </input>
              <label htmlFor="choice1_deux">Madame</label>
            </div>
            <div className="boxsizing margin-b20 margin-t10px">
                <input type="radio"
                       id="choice2_deux"
                       onChange={this.handleChange}
                       name="deux_civilite"
                       value="monsieur">
                </input>
                <label htmlFor="choice2_deux">Monsieur</label>
            </div>

            <div className="container_form_form1 _3">
            <div className="">
                <input className="input_100 boxsizing"
                       onChange={this.handleChange}
                       type="text"
                       name="deux_nom"
                       placeholder="NOM">
                </input>
            </div>
            <div className="">
                <input className="input_100 boxsizing"
                       onChange={this.handleChange}
                       type="text"
                       name="deux_prenom"
                       placeholder="PRÉNOM">
                </input>
            </div>

            <div className="">
                <input className="boxsizing w-centpcent"
                       onChange={this.handleChange}
                       type="text"
                       name="deux_tel"
                       placeholder="Numéro de téléphone">
                </input>
            </div>
            <div className="">
                <input className="boxsizing w-centpcent"
                       onChange={this.handleChange}
                       type="email"
                       name="deux_email"
                       placeholder="EMAIL">
                </input>
            </div>
            <div className="">
                <input className="boxsizing" id="adress_dif"
                       onChange={this.props.onClickAdresseDif} type="checkbox"></input>
                <label htmlFor="adress_dif">Son adresse est différente de la vôtre </label>
            </div>
            {adresseSup}
        </div>
        </div>
    );
  }
}
export default DeuxiemeParticulier;
