import React, { Component } from "react";



class DeuxiemeParticulierAdresseSup extends Component {

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }

  render() {
    return(
      <div>
          <div className="space_between">
              <input className="w-quarantecinqpcent"
                     name="deux_batiment"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Bâtiment">
              </input>
              <input className="w-quarantecinqpcent"
                     name="deux_entree"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Entrée">
              </input>
          </div>
          <div className="space_between">
              <input className="w-quarantecinqpcent"
                     name="deux_appart"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Appartement">
              </input>
              <input className="w-quarantecinqpcent"
                     name="deux_etage"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Etage">
              </input>
          </div>
          <div className="">
            <input className="boxsizing w-centpcent"
                   name="deux_complement"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Complément d’adresse">
            </input>
          </div>
          <div className=" space_between">
              <input className="boxsizing"
                     name="deux_numero"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="N°">
              </input>
              <input className="boxsizing w-quatrevingtdixpcent"
                     name="deux_rue"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Nom de la rue / Voie / Chemin">
              </input>
          </div>
          <div className="space_between">
              <input className="w-quarantecinqpcent"
                     name="deux_code_postal"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Code postal">
              </input>
              <input className="w-quarantecinqpcent"
                     name="deux_ville"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Ville">
              </input>
          </div>
            <div className="">
              <input className="boxsizing"
                     name="deux_pays"
                     onChange={this.handleChange}
                     type="text"
                     placeholder="Pays">
              </input>
            </div>
      </div>
    );
  }
}
export default DeuxiemeParticulierAdresseSup;
