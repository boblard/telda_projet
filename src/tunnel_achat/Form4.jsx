import React, { Component } from "react";
import { Link, DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

import mastercard from "../images/mastercard.png";
import cartebancaire from "../images/cartebancaire.png";
import visa from "../images/visa.png";
import paypal from "../images/paypal.png";


class Form4 extends Component {
  render() {
    return (
      <div className="display">
        <div className="container_form_form1 margin-b20 _2 t-a-l padding_l2px">
          <p className="paragraphe1">PROCÉDER AU PAIEMENT</p>
        </div>
        <div className="boxsizing margin-t2vh">
          <div>
            <input
              type="checkbox"
              onChange={this.props.onChange}
              id="choix1"
              name="CGU"

            />
            <label htmlFor="choix1">
              J'ai lu les Conditions Générales de Vente et d'Utilisation
            </label>
          </div>
        </div>
        <div className="boxsizing margin-t2vh">
          <div>
            <input
              type="checkbox"
              id="choix2"
              onChange={this.props.onChange}
              name="CGVU"
            />
            <label htmlFor="choix2">
              J'accepte les Conditions Générales de Vente et d'Utilisation
            </label>
          </div>
        </div>
        <div className="container_form_form4_paiement_1">
          <p>Choisissez votre mode de paiement</p>
        </div>
        <div className="display_paiement">
          <div>
            <input
              type="radio"
              id="buttonRadioP"
              name="buttonRadio"
              value="PayPal"
            />
          <img src={paypal} alt="Logo PayPal" id="img_paiement" />
          </div>
          <div className="">
            <input
              type="radio"
              id="buttonRadioP"
              name="buttonRadio"
              value="CB"
            />
          <img src={cartebancaire} alt="Logo CarteBancaire" id="img_paiement" />
          </div>
          <div className="">
            <input
              type="radio"
              id="buttonRadioP"
              name="buttonRadio"
              value="Visa"
            />
          <img src={visa} alt="Logo Visa" id="img_paiement" />
          </div>
          <div>
            <input
              type="radio"
              id="buttonRadioP"
              name="buttonRadio"
              value="Mastercard"
            />
            <img src={mastercard} alt="Logo Mastercard" id="img_paiement" />
          </div>
        </div>
        <div className="w-quatrevingtdixpcent displayflex_center">
          <button type="submit">VALIDER</button>
        </div>
      </div>
    );
  }
}
export default Form4;
