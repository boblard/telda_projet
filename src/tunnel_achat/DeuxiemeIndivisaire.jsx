import React, { Component } from "react";
import DeuxiemeIndivisaireAdresseSup from "./DeuxiemeIndivisaireAdresseSup"

class DeuxiemeIndivisaire extends Component{

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }

      render(){
        var adresseIndivisaireSup;

         if(this.props.prevState.adresse_dif === true){
           adresseIndivisaireSup = <DeuxiemeIndivisaireAdresseSup onChange={this.props.onChange} onClickAdresseDif={this.props.onClickAdresseDif} />;
           } else {
             adresseIndivisaireSup = null;
           };
        return(
        <div>
            <div className="margin-t10px">
             <p className="blue_color">Indivisaire supplémentaire :  </p>
                <input
                    className="input_100 boxsizing"
                    name="deux_nom"
                    onChange={this.handleChange}
                    type="text"
                    placeholder="NOM" required>
                </input>

            </div>
             <div className="margin-t10px">
                 <input
                    className="input_100 boxsizing"
                    name="deux_prenom"
                    onChange={this.handleChange}
                    type="text"
                    placeholder="PRÉNOM" required>
                </input>
              </div>
              <div className="space_between">
                  <input className="w-quarantecinqpcent"
                         name="deux_ddn"
                         onChange={this.handleChange}
                         type="date"
                         placeholder="Date de naissance">
                  </input>
                  <input className="w-quarantecinqpcent"
                         name="deux_vdn"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Lieu de naissance">
                  </input>
              </div>
              <div className="space_between">
                  <input className="w-quarantecinqpcent"
                         name="deux_telephone"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="N° téléphone"required>
                  </input>
                  <input className="w-quarantecinqpcent"
                         name="deux_email"
                         onChange={this.handleChange}
                         type="text"
                         placeholder="Email"required>
                  </input>
              </div>
            <div className="">
                  <input
                    className="boxsizing"
                    id="adress_dif"
                    onChange={this.props.onClickAdresseDif}
                    type="checkbox"></input>
                   <label htmlFor="adress_dif">Son adresse est différente de la vôtre </label>
             </div>
                   {adresseIndivisaireSup}
          </div>

                )
            }
}
export default DeuxiemeIndivisaire;
