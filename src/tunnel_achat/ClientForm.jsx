import React, { Component } from "react";
import Form1 from "./Form1";
import Form2 from "./Form2";
import Form3 from "./Form3";
import Form4 from "./Form4";
import ResumeDevis from "./ResumeDevis";
import Form4Validation from "./Form4Validation";
import '../tunnel_achat/index.css';

class ClientForm extends Component {

state = {
  forms: [
    {id: "0", form_object: ""}
  ]
};

handleAdd = form => {
  //MAJ du state
//copie
const forms = [...this.state.forms];
//J'ajoute mes nouvelles données dans le tableau forms
forms.push(form);
//et je mete à jour le state forms
this.setState({ forms });
console.log("Ajouté");
};

  render() {
    console.log("etat de ClientForm");
    console.log(this.state);
    const currentId = this.state.forms[this.state.forms.length - 1].id;
    var currentForm;

    if (currentId == 0){
    currentForm = <Form1 id={currentId} onForm1Add={this.handleAdd} />;
    }
    else if (currentId == 1) {
      currentForm = <Form2 id={currentId} onForm1Add={this.handleAdd} />;
    }
    else if (currentId == 2) {
      currentForm = <Form3 id={currentId} onForm1Add={this.handleAdd} />;
    }
    else if (currentId == 3) {
      currentForm = <Form4 id={currentId} onForm1Add={this.handleAdd} />;
    }


    return (
      <div className="container_form">
        <div className="container_form_left">
          {currentForm}
        </div>
        <div className="container_form_right">
          <ResumeDevis id={currentId} prevState={this.state}/>
        </div>
      </div>
    );
  }
}
export default ClientForm;
