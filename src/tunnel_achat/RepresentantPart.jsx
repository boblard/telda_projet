import React , { Component} from "react";

import DeuxiemeAssocie from "./DeuxiemeAssocie"


class RepresentantPart extends Component {
    render() {
        return (
            <div className="container_input_local">
                <div className="boxsizing ">
                    <input type="radio"
                           id="choice4"
                           onChange={this.handleChange}
                           name="civilite"
                           value="madame"></input>
                    <label htmlFor="choice4">Madame</label>
                </div>
                <div className="boxsizing margin-b20 margin-t10px">
                    <input type="radio"
                           id="choice5"
                           onChange={this.handleChange}
                           name="civilite"
                           value="monsieur"></input>
                    <label htmlFor="choice5">Monsieur</label>
                </div>
                <div className="container_form_form1 _3">
                    <div className="">
                        <input className="input_100 boxsizing" name="nom" onChange={this.handleChange} type="text"
                               placeholder="NOM"></input>
                    </div>
                    <div className="">
                        <input className="input_100 boxsizing" name="prenom" onChange={this.handleChange} type="text"
                               placeholder="Prénom"></input>
                    </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent" name="ddn" onChange={this.handleChange} type="date"
                               placeholder="Date de naissance"></input>
                        <input className="w-quarantecinqpcent" name="vdn" onChange={this.handleChange} type="text"
                               placeholder="Lieu de naissance"></input>
                    </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent" name="n°_telephone" onChange={this.handleChange}
                               type="text" placeholder="N° téléphone"></input>
                        <input className="w-quarantecinqpcent" name="email" onChange={this.handleChange} type="text"
                               placeholder="Email"></input>
                    </div>
                    <p className="blue_color padding_5px">Votre adresse postale</p>
                    <div className="">
                        <input className="boxsizing w-centpcent" name="residence" onChange={this.handleChange}
                               type="text" placeholder="Nom de la résidence"></input>
                    </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent" name="batiment" onChange={this.handleChange} type="text"
                               placeholder="Bâtiment"></input>
                        <input className="w-quarantecinqpcent" name="entree" onChange={this.handleChange} type="text"
                               placeholder="Entrée"></input>
                    </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent" name="appart" onChange={this.handleChange} type="text"
                               placeholder="Appartement"></input>
                        <input className="w-quarantecinqpcent" name="etage" onChange={this.handleChange} type="text"
                               placeholder="Etage"></input>
                    </div>
                    <div className="">
                        <input className="boxsizing w-centpcent" name="complement" onChange={this.handleChange}
                               type="text" placeholder="Complément d’adresse"></input>
                    </div>
                    <div className=" space_between">
                        <input className="boxsizing" name="numero" onChange={this.handleChange} type="text"
                               placeholder="N°"></input>
                        <input className="boxsizing w-quatrevingtdixpcent" name="rue" onChange={this.handleChange}
                               type="text" placeholder="NOM DE LA RUE / VOIE / CHEMIN"></input>
                    </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent" name="code_postal" onChange={this.handleChange}
                               type="text" placeholder="Code postal"></input>
                        <input className="w-quarantecinqpcent" name="ville" onChange={this.handleChange} type="text"
                               placeholder="Ville"></input>
                    </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent" name="pays" onChange={this.handleChange} type="text"
                               placeholder="Pays"></input>
                    </div>
                </div>
            </div>

        );
    }
}
export default RepresentantPart;