import React, { Component } from "react";


class Form4Validation extends Component {
  render() {
    return (
      <div>
        <div className="container_form_form1 _2">
          <p>COMPTE CRÉE</p>
        </div>
        <div className="container_blue">
          <p>
            Votre facture acquittée a été envoyé dans votre boîte mail.
            <br />
            Vous en retrouverez un exemplaire téléchargeable dans le tableau de bord de votre bien, rubrique Factures récupérables / Frais de gestion.
          </p>
        </div>
        <div className="container_blue">
          <p>
            RENDEZ-VOUS dès maintenant sur votre espace personnel, et commencez
            votre nouvelle expérience de gestionnaire !
          </p>
        </div>
        <div className="text_center">
          <div className="container_button">
          <button type="submit" id="buttonForm4" onSubmit={this.props.onSubmit}>J'accède à mon bureau maintenant</button>
          </div>
        </div>
          <p className="padding_left_vingtcinqpourcent ">OU</p>
          <div className="text_center">
            <div className="container_button">
            <button type="submit" id="buttonForm4" onSubmit={this.props.onSubmit}>Enregistrez un nouveau bien</button>
          </div>
        </div>
      </div>
    );
  }
}
export default Form4Validation;
