import React, { Component } from "react";

import FormRepresentantPAssocie from "./FormRepresentantPAssocie";
import FormRepresentantSAssocie from "./FormRepresentantSAssocie";

class DeuxiemeAssocie extends Component{

    render(){
      var typeRepresentant = this.props.prevState.type_representant_associe_sup;
      var typeFormulaire;
      if (typeRepresentant === "particulier"){
         typeFormulaire = <FormRepresentantPAssocie onChange={this.props.onChange} />;
      }
      else if (typeRepresentant === "societe") {
         typeFormulaire = <FormRepresentantSAssocie onChange={this.props.onChange} />;
      }

        return(
            <div className="margin-t10px">

                <p className="label_blue margin-t2vh margin-b20">Société représentée par : </p>
                <div className="display_row">
                    <div className="display_column">
                        <div className="boxsizing">
                            <input
                                type="radio"
                                id="rep_particulier_associe"
                                onChange={this.props.onClickTypeRepresentantAssocie}
                                name="type_representant_associe_sup"
                                value="particulier">
                            </input>
                            <label htmlFor="rep_particulier_associe">Un particulier</label>
                        </div>
                    </div>
                    <div className="display_column">
                        <div className="boxsizing">
                            <input
                                type="radio"
                                id="rep_ste_associe"
                                onChange={this.props.onClickTypeRepresentantAssocie}
                                name="type_representant_associe_sup"
                                value="societe">
                            </input>
                            <label htmlFor="rep_ste_associe">Une société</label>
                        </div>
                    </div>
                </div>
                {typeFormulaire}
            </div>
        )
    }
}


export default DeuxiemeAssocie;
