import React, { Component } from "react";
import { Link, DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'


class Form2 extends Component {

  state = {};

  handleChange = event => {
    this.setState({[event.currentTarget.name]: event.currentTarget.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    scroll.scrollToTop();
    const currentMaxId = this.props.id;

    console.log("hello");

    const id = Number(currentMaxId + 1);
    console.log(id);
    const form_object = this.state;

    this.props.onForm1Add({ id, form_object });
    this.setState({ });

  };

  render(){
    console.log("Form2 :");
    console.log(this.state);
    return (
      <form onSubmit={this.handleSubmit}>
      <div className="display">
        <div className="container_blue padding_r_20p">
          <p>Pour enregistrer votre 1er bien loué, vous aurez besoin des mêmes
             informations à figurer au bail de location.
          <br />A la fin de votre règlement, une facture de gestion propre à ce bien sera
             disponible à l’impression, et classée dans porte document numérique de
             l’année fiscale 2019
          </p>
        </div>
        <div className="container_form_form4_1 _2">
            <p>LE TYPE DE BIEN LOUÉ,</p>
            <p>(FAISANT L'OBJET D'UN SEUL CONTRAT DE BAIL)</p>
        </div>
        <div className="container_form_form1 _3">
          <div>
              <input type="radio"
                     id="choix1"
                     onChange={this.handleChange}
                     name="type_habitation"
                     value="maison">
              </input>
              <label htmlFor="choix1">Une Maison</label>
          </div>
          <div>
              <input type="radio"
                     id="choix2"
                     onChange={this.handleChange}
                     name="type_habitation"
                     value="appartement">
              </input>
              <label htmlFor="choix2">Un appartement en copropriété</label>
          </div>
          <div>
              <input type="radio"
                     id="choix3"
                     onChange={this.handleChange}
                     name="type_habitation"
                     value="lot">
              </input>
                   <label htmlFor="choix3">Un lot d’habitation dans votre immeuble</label>
          </div>
        </div>
        <div className="container_blue padding_r_20p">
          <p className="underline">J'ai un appartement + un parking dans la même résidence ?
          <br /> Choisir 1 ou 2 contrats de bail ?
          </p>
        </div>
        <div className="container_input_local">
        <div className="container_form_form1 _3">
            <p className="blue_color">Renseignez l'adresse du bien :</p>
          <div className="">
            <input className="boxsizing w-centpcent"
                   name="residence_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Nom de la résidence">
            </input>
          </div>
          <div className="space_between">
            <input className="w-quarantecinqpcent"
                   name="batiment_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Bâtiment">
            </input>
            <input className="w-quarantecinqpcent"
                   name="entree_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Entrée">
            </input>
          </div>
          <div className="space_between">
            <input className="w-quarantecinqpcent"
                   name="appart_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Appartement">
            </input>
            <input className="w-quarantecinqpcent"
                   name="etage_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Etage">
            </input>
          </div>
          <div className="">
            <input className="boxsizing w-centpcent"
                   name="complement_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Complément d’adresse">
            </input>
          </div>
          <div className=" space_between">
            <input className="boxsizing" name="numero_bien" onChange={this.handleChange} type="text" placeholder="N°"></input>
            <input className="boxsizing w-quatrevingtdixpcent" name="rue_bien" onChange={this.handleChange} type="text" placeholder="NOM DE LA RUE / VOIE / CHEMIN"></input>
          </div>
          <div className="space_between">
            <input className="w-quarantecinqpcent"
                   name="code_postal_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Code postal">
            </input>
            <input className="w-quarantecinqpcent"
                   name="ville_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Ville">
            </input>
          </div>
          <div className="space_between">
            <input className="w-quarantecinqpcent"
                   name="pays_bien"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Pays">
            </input>
          </div>
        <div className="container_button displayflex_center">
          <button type="submit">CONTINUER</button>
        </div>
        </div>
      </div>
      </div>
    </form>
    );
  }
}
export default Form2;
