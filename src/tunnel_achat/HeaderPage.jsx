import React, { Component } from "react";
import "./HeaderPage.css"
class HeaderPage extends Component {

  render(){
    return (
      <header>
        <div className="indicators"><p>1 - COORDONNÉES PROPRÉTAIRE(E)</p></div>
        <div className="indicators"><p>2 - VOTRE BIEN</p></div>
        <div className="indicators"><p>3 - CALCUL DE VOTRE ABONNEMENT ANNUEL</p></div>
        <div className="indicators"><p>4 - VALIDATION</p></div>
      </header>
    );
  }
}
export default HeaderPage;
