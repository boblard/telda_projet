import React, { Component } from "react";


class FormRepresentantP extends Component {

  handleChange = event => {
    this.props.onChange(event.currentTarget);
  }


  render(){
    return(
      <div>
        <div className="boxsizing">
            <input
                type="radio"
                id="associe_male"
                name="civilite_associe_rep_part"
                onChange={this.handleChange}
                value="monsieur">
            </input>
                <label htmlFor="associe_male">Monsieur</label>
        </div>
        <div className="boxsizing">
            <input
                type="radio"
                id="associe_female"
                name="civilite_associe_rep_part"
                onChange={this.handleChange}
                value="madame">
            </input>
                <label htmlFor="associe_female">Madame</label>
        </div>
        <div className="boxsizing">
            <input
                className="input_100 boxsizing"
                name="nom_associe_rep_part"
                onChange={this.handleChange}
                type="text"
                placeholder="Nom">
            </input>
        </div>
        <div className="boxsizing">
            <input
                className="input_100 boxsizing"
                name="prenom_associe_rep_part"
                onChange={this.handleChange}
                type="text"
                placeholder="Prénom">
            </input>
        </div>
        <div className="space_between">
            <input className="w-quarantecinqpcent"
                   name="ddn_associe_rep_part"
                   onChange={this.handleChange}
                   type="date"
                   placeholder="Date de naissance">
            </input>
            <input className="w-quarantecinqpcent"
                   name="vdn_associe_rep_part"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Lieu de naissance">
            </input>
        </div>
        <div className="space_between">
            <input className="w-quarantecinqpcent"
                   name="telephone_associe_rep_part"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="N° téléphone">
            </input>
            <input className="w-quarantecinqpcent"
                   name="email_associe_rep_part"
                   onChange={this.handleChange}
                   type="text"
                   placeholder="Email">
            </input>
        </div>
        <div className="boxsizing">
            <p className="blue_color padding_5px">Votre adresse postale</p>
        </div>
        <div className="">
            <input
                className="boxsizing w-centpcent"
                name="residence_associe_rep_part"
                onChange={this.handleChange}
                type="text"
                placeholder="Nom de la résidence">
            </input>
        </div>
        <div className="space_between">
          <input className="w-quarantecinqpcent"
                 name="batiment_associe_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Bâtiment">
          </input>
          <input className="w-quarantecinqpcent"
                 name="entree_associe_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Entrée">
          </input>
        </div>
        <div className="space_between">
          <input className="w-quarantecinqpcent"
                 name="appart_associe_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Appartement">
          </input>
          <input className="w-quarantecinqpcent"
                 name="etage_associe_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Etage">
          </input>
        </div>
        <div className="space_between">
          <input className="w-quarantecinqpcent"
                 name="code_postal_associe_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Code postal">
          </input>
          <input className="w-quarantecinqpcent"
                 name="ville_associe_rep_part"
                 onChange={this.handleChange}
                 type="text"
                 placeholder="Ville">
          </input>
        </div>
        <div className="">
            <input
                className="boxsizing"
                name="pays_associe_rep_part"
                onChange={this.handleChange}
                type="text"
                placeholder="Pays">
            </input>
        </div>
      </div>
    )
  }
}
export default FormRepresentantP;
