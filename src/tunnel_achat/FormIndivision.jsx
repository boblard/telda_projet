import React, { Component } from "react";
import DeuxiemeIndivisaire from "./DeuxiemeIndivisaire";

class FormIndivision extends Component {

    handleChange = event => {
      this.props.onChange(event.currentTarget);
    }

    render() {
    var indivSup;
    var textButton = "Ajouter un indivisaire";
    if(this.props.prevState.deuxIndiv === true){
        indivSup = <DeuxiemeIndivisaire prevState={this.props.prevState} onClickAdresseDif={this.props.onClickAdresseDif} onChange={this.props.onChange} />;
        textButton = "Annuler";
    }else {
        indivSup = null;
    }

        return(
            <form onSubmit={this.props.onSubmit}>
                <div className="padding_l20p">
                    <div className="container_input_local">
                        <div className="container_form_form1 _3">
                        <div className="boxsizing ">
                            <input type="radio"
                                   id="civilite"
                                   onChange={this.handleChange}
                                   name="civilite"
                                   value="madame">
                            </input>
                            <label htmlFor="civilite">Madame</label>
                        </div>
                        <div className="boxsizing margin-b20 margin-t10px">
                            <input type="radio"
                                   id="civilite2"
                                   onChange={this.handleChange}
                                   name="civilite"
                                   value="monsieur">
                            </input>
                            <label htmlFor="civilite2">Monsieur</label>
                        </div>
                        <div className="container_form_form1 _3">
                            <div className="">
                                <input
                                    className="input_100 boxsizing"
                                    name="nom"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="NOM">
                                </input>
                            </div>
                            <div className="">
                                <input
                                    className="input_100 boxsizing"
                                    name="prenom"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="PRÉNOM">
                                </input>
                            </div>
                            <div className="space_between">
                                <input className="w-quarantecinqpcent"
                                       name="ddn"
                                       onChange={this.handleChange}
                                       type="date"
                                       placeholder="Date de naissance">
                                </input>
                                <input className="w-quarantecinqpcent"
                                       name="vdn"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Lieu de naissance">
                                </input>
                            </div>
                            <div className="space_between">
                                <input className="w-quarantecinqpcent"
                                       name="n°_telephone"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="N° téléphone">
                                </input>
                                <input className="w-quarantecinqpcent"
                                       name="email"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Email">
                                </input>
                            </div>
                            <p className="blue_color padding_5px">Votre adresse postale</p>
                            <div className="">
                                <input
                                    className="boxsizing w-centpcent"
                                    name="residence"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="Nom de la résidence">
                                </input>
                            </div>
                            <div className="space_between">
                                <input className="w-quarantecinqpcent"
                                       name="batiment"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Bâtiment">
                                </input>
                                <input className="w-quarantecinqpcent"
                                       name="entree"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Entrée">
                                </input>
                            </div>
                            <div className="space_between">
                                <input className="w-quarantecinqpcent"
                                       name="appart"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Appartement">
                                </input>
                                <input className="w-quarantecinqpcent"
                                       name="etage"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Etage">
                                </input>
                            </div>
                            <div className="space_between">
                                <input className="w-quarantecinqpcent"
                                       name="code_postal"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Code postal">
                                </input>
                                <input className="w-quarantecinqpcent"
                                       name="ville"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Ville">
                                </input>
                            </div>
                            <div className="">
                                <input
                                      className="boxsizing"
                                      name="pays"
                                      onChange={this.handleChange}
                                      type="text"
                                      placeholder="Pays">

                                </input>
                            </div>
                            <p className="blue_color ">Vous avez la possibilité de partager les informations avec les autres indivisaires: </p>
                            <div className="container_button displayflex_center">
                                <button type="button" id="new_prop" onClick={this.props.onClickDeuxIndiv}>{textButton}</button>
                            </div>
                            {indivSup}
                            <div className="container_button displayflex_center">
                                <button type="submit">CONTINUER</button>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </form>

        )
    }
}
export default FormIndivision;
