import React, { Component } from "react";
import DeuxiemeAssocie from "./DeuxiemeAssocie";
import FormRepresentantP from "./FormRepresentantP";
import FormRepresentantS from "./FormRepresentantS";

class FormSci extends Component {

    handleChange = event => {
      this.props.onChange(event.currentTarget);
    }

    render() {
// Affichage deuxieme associé
      var associeSup;
      var textButton2 = "Ajouter un associé";
      if(this.props.prevState.deuxAssocie == true){
          associeSup = <DeuxiemeAssocie onChange={this.props.onChange} prevState={this.props.prevState} onClickTypeRepresentantAssocie={this.props.onClickTypeRepresentantAssocie} />;
          textButton2 = "Annuler";
      }else {
          associeSup = null;
      }
// Affichage representant société ou particulier
        var typeRepresentant = this.props.prevState.type_representant;
        var typeFormulaire;
        if (typeRepresentant === "particulier"){
           typeFormulaire = <FormRepresentantP onChange={this.props.onChange} />;
      }
        else if (typeRepresentant === "societe") {
           typeFormulaire = <FormRepresentantS onChange={this.props.onChange} />;
        }
// Affichage input pour la TVA
        var tva_OK = this.props.prevState.tva_ok;
        var tva;
        if (tva_OK === true){
          console.log("OKLM");
           tva = <div className="boxsizing padding_l20p margin-t2vh"><input className="boxsizing" name="num_tva_societe" onChange={this.handleChange} type="texte" placeholder="Numéro de T.V.A. :"></input></div>;
      }
        return (
      <form onSubmit={this.props.onSubmit}>
        <div className="padding_l20p">
            <div className="container_input_local">
                <div className="container_form_form1 _3">
                    <div className="boxsizing">
                        <input
                            className="input_100 boxsizing"
                            name="denomination"
                            onChange={this.handleChange}
                            type="text"
                            placeholder="Dénomination de la société">
                        </input>
                    </div>
                    <div className="display_row margin-b20">
                        <div className="display_column">
                            <div className="boxsizing">
                                <input
                                    type="radio"
                                    id="sci"
                                    name="type_societe"
                                    onChange={this.handleChange}
                                    value="sci">
                                </input>
                                <label htmlFor="sci">SCI</label>
                            </div>
                            <div className="boxsizing">
                                <input
                                    type="radio"
                                    id="sarl"
                                    name="type_societe"
                                    onChange={this.handleChange}
                                    value="sarl">
                                </input>
                                <label htmlFor="sarl">SARL familiale</label>
                            </div>
                        </div>
                        <div className="display_column">
                            <div className="boxsizing">
                                <input
                                    type="radio"
                                    id="scpi"
                                    name="type_societe"
                                    onChange={this.handleChange}
                                    value="scpi">
                                </input>
                                    <label htmlFor="scpi">SCPI</label>
                            </div>
                            <div className="boxsizing">
                                <input
                                    type="radio"
                                    id="autre"
                                    onChange={this.handleChange}
                                    name="type_societe"
                                    value="autre">
                                </input>
                                    <label htmlFor="autre">Autre</label>
                            </div>
                        </div>
                    </div>
                            <div className="boxsizing">
                                <input
                                    className="input_100 boxsizing"
                                    name="nom_societe"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="Nom de la Société">
                                </input>
                            </div>
                                <p className="blue_color padding_5px">Adresse postale de la société</p>
                            <div className="">
                                <input
                                    className="boxsizing w-centpcent"
                                    name="residence_societe"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="Nom de la résidence">
                                </input>
                            </div>
                            <div className="">
                                <input
                                    className="boxsizing w-centpcent"
                                    name="complement_societe"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="Complément d’adresse">
                                </input>
                            </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent"
                               name="batiment_societe"
                               onChange={this.handleChange}
                               type="text"
                               placeholder="Bâtiment">
                        </input>
                        <input className="w-quarantecinqpcent"
                               name="entree_societe"
                               onChange={this.handleChange}
                               type="text"
                               placeholder="Entrée">
                        </input>
                    </div>
                    <div className="space_between">
                        <input className="w-quarantecinqpcent"
                               name="appart_societe"
                               onChange={this.handleChange}
                               type="text"
                               placeholder="Appartement">
                        </input>
                        <input className="w-quarantecinqpcent"
                               name="etage_societe"
                               onChange={this.handleChange}
                               type="text"
                               placeholder="Etage">
                        </input>
                    </div>
                        <div className="">
                            <div className="space_between_3">
                                <input
                                    className="boxsizing"
                                    name="bp_societe"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="BP ou CS ">
                                </input>
                            </div>
                            <div className="space_between">
                                <input className="w-quarantecinqpcent"
                                       name="code_postal_societe"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Code postal">
                                </input>
                                <input className="w-quarantecinqpcent"
                                       name="ville_societe"
                                       onChange={this.handleChange}
                                       type="text"
                                       placeholder="Ville">
                                </input>
                            </div>
                            <div className="space_between">
                                <input
                                    className="boxsizing"
                                    name="pays_societe"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="Pays">
                                </input>
                            </div>
                            </div>
                            <p className="blue_color padding_5px">Identifiant de la société</p>
                            <div className="space_between">
                                <input
                                    className="input_100 boxsizing"
                                    name="siret_societe"
                                    type="text"
                                    onChange={this.handleChange}
                                    placeholder="Numéro de SIRET">
                                </input>
                            </div>
                            <div className="space_between">
                                <input
                                    className="w-quarantecinqpcent"
                                    name="rcs_societe"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="RCS">
                                </input>
                                <input
                                    className="w-quarantecinqpcent"
                                    name="ville_rcs_societe"
                                    onChange={this.handleChange}
                                    type="text"
                                    placeholder="Ville">
                                </input>
                            </div>

                            <div className="boxsizing margin-t1vh">
                              <input
                                   className="boxsizing"
                                   type="checkbox"
                                   id="tva"
                                   onChange={this.props.onClickTVA}
                                   value="tva">
                               </input>
                            <label htmlFor="tva">La société est soumise à la T.V.A.</label>
                            </div>
                            {tva}
                            <div className="boxsizing margin-t1vh">
                                <label onClick={e => window.alert("Cette fonctionnalité est en cours de préparation, suivez notre avancement sur Facebook. A très bientôt !")} htmlFor="tva2">Vous appelez les loyers et charges avec T.V.A. ?</label>
                            </div>
                            <p className="label_blue margin-t2vh margin-b20">Société représentée par : </p>
                    <div className="display_row">
                                <div className="boxsizing">
                                    <input
                                        type="radio"
                                        id="rep_particulier"
                                        onChange={this.props.onClickTypeRepresentant}
                                        name="type_representant"
                                        value="particulier">
                                    </input>
                                    <label htmlFor="rep_particulier">Un particulier</label>
                                </div>
                                <div className="boxsizing">
                                    <input
                                        type="radio"
                                        id="rep_ste"
                                        onChange={this.props.onClickTypeRepresentant}
                                        name="type_representant"
                                        value="societe">
                                    </input>
                                    <label htmlFor="rep_ste">Une société</label>
                                </div>
                        </div>
                        {typeFormulaire}
                    <p className="blue_color">Vous avez la possibilité de partager les informations avec les autres associés :</p>
                    <div className="container_button displayflex_center">
                        <button type="button" id="new_prop" onClick={this.props.onClickDeuxAssocie}>{textButton2}</button>
                    </div>
                    {associeSup}
                    <div className="container_button displayflex_center">
                        <button type="submit">CONTINUER</button>
                    </div>
                </div>
            </div>
        </div>
      </form>
        )
    }
}
export default FormSci;
