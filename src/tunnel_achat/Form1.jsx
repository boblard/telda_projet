import React, { Component } from "react";

import FormParticulier from "./FormParticulier";
import FormSci from "./FormSci";
import FormIndivision from "./FormIndivision";
import { Link, DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

const initialState = {
  civilite: undefined,
  nom: undefined,
  prenom: undefined,
  residence: undefined,
  infos_batiment: undefined,
  complement: undefined,
  numero: undefined,
  rue: undefined,
  code_postal: undefined,
  ville: undefined,
  pays: undefined,
  telephone: undefined,
  email: undefined,
  deuxProprio: undefined,
  deux_civilite: undefined,
  deux_nom: undefined,
  deux_prenom: undefined,
  deux_ddn: undefined,
  deux_vdn: undefined,
  deux_tel: undefined,
  deux_email: undefined,
  adresse_dif: undefined,
  deux_residence: undefined,
  deux_infos_batiment: undefined,
  deux_complement: undefined,
  deux_batiment: undefined,
  deux_entree: undefined,
  deux_numero: undefined,
  deux_rue: undefined,
  deux_etage: undefined,
  deux_code_postal: undefined,
  deux_appart: undefined,
  deux_ville: undefined,
  deux_pays: undefined,
  deux_telephone: undefined,
  deuxAssocie: undefined,

  //FormSci
  num_tva_societe: undefined,
  denomination: undefined,
  type_societe: undefined,
  nom_societe: undefined,
  residence_societe: undefined,
  complement_societe: undefined,
  batiment_societe: undefined,
  entree_societe: undefined,
  appart_societe: undefined,
  etage_societe: undefined,
  bp_societe: undefined,
  boite_postale_societe: undefined,
  code_postal_societe: undefined,
  ville_societe: undefined,
  pays_societe: undefined,
  siret_societe: undefined,
  rcs_societe: undefined,
  ville_rcs_societe: undefined,
  type_representant: undefined,

  //FormRepresentantP
  civilite_ste_rep_part: undefined,
  nom_ste_rep_part: undefined,
  prenom_ste_rep_part: undefined,
  ddn_ste_rep_part: undefined,
  vdn_ste_rep_part: undefined,
  telephone_ste_rep_part: undefined,
  email_ste_rep_part: undefined,
  residence_ste_rep_part: undefined,
  batiment_ste_rep_part: undefined,
  entree_ste_rep_part: undefined,
  appart_ste_rep_part: undefined,
  etage_ste_rep_part: undefined,
  code_postal_ste_rep_part: undefined,
  ville_ste_rep_part: undefined,
  pays_ste_rep_part: undefined,

  //FormRepresentantS
  type_ste_rep_soc: undefined,
  denomination_ste_rep_soc: undefined,
  nom_ste_rep_soc: undefined,
  residence_ste_rep_soc: undefined,
  batiment_ste_rep_soc: undefined,
  entree_ste_rep_soc: undefined,
  appart_ste_rep_soc: undefined,
  etage_ste_rep_soc: undefined,
  boite_postale_ste_rep_soc: undefined,
  code_postal_ste_rep_soc: undefined,
  ville_ste_rep_soc: undefined,
  pays_ste_rep_soc: undefined,
  civilite_ste_rep_soc_rep: undefined,
  nom_ste_rep_soc_rep: undefined,
  prenom_ste_rep_soc_rep: undefined,
  ddn_ste_rep_soc_rep: undefined,
  vdn_ste_rep_soc_rep: undefined,
  telephone_ste_rep_soc_rep: undefined,
  email_ste_rep_soc_rep: undefined,

  //DeuxiemeAssocie
  type_representant_associe_sup: undefined,

  //FormRepresentantP_Associe
  civilite_associe_rep_part: undefined,
  nom_associe_rep_part: undefined,
  prenom_associe_rep_part: undefined,
  ddn_associe_rep_part: undefined,
  vdn_associe_rep_part: undefined,
  telephone_associe_rep_part: undefined,
  email_associe_rep_part: undefined,
  residence_associe_rep_part: undefined,
  batiment_associe_rep_part: undefined,
  entree_associe_rep_part: undefined,
  appart_associe_rep_part: undefined,
  etage_associe_rep_part: undefined,
  code_postal_associe_rep_part: undefined,
  ville_associe_rep_part: undefined,
  pays_associe_rep_part: undefined,

  //FormFormRepresentantS_Associe
  type_associe_rep_soc: undefined,
  denomination_associe_rep_soc: undefined,
  nomsoc_associe_rep_soc: undefined,
  residence_associe_rep_soc: undefined,
  batiment_associe_rep_soc: undefined,
  entree_associe_rep_soc: undefined,
  appart_associe_rep_soc: undefined,
  etage_associe_rep_soc: undefined,
  boite_postale_associe_rep_soc: undefined,
  code_postal_associe_rep_soc: undefined,
  ville_associe_rep_soc: undefined,
  pays_associe_rep_soc: undefined,
  civilite_associe_rep_soc: undefined,
  nom_associe_rep_soc: undefined,
  prenom_associe_rep_soc: undefined,
  ddn_associe_rep_soc: undefined,
  vdn_associe_rep_soc: undefined,
  telephone_associe_rep_soc: undefined,
  email_associe_rep_soc: undefined,
};

class Form1 extends Component {
  constructor(props) {
      super(props)
      this.state = initialState;
  }
  reset() {
      this.setState(initialState);
  }

  handleAdd = element => {
    this.setState({[element.name]: element.value});
  }

  handleClickChoiceTypePersonne = choice => {
    this.reset();
    this.setState({type_personne: choice.target.value});
  }
  handleClickChoiceTypeRepresentant = choice => {
    this.setState({civilite_ste_rep_part: undefined,nom_ste_rep_part: undefined,prénom_ste_rep_part: undefined,ddn_ste_rep_part: undefined,vdn_ste_rep_part: undefined,telephone_ste_rep_part: undefined,email_ste_rep_part: undefined,residence_ste_rep_part: undefined,batiment_ste_rep_part: undefined,entree_ste_rep_part: undefined,appart_ste_rep_part: undefined,etage_ste_rep_part: undefined,code_postal_ste_rep_part: undefined,ville_ste_rep_part: undefined,pays_ste_rep_part:                          undefined,type_ste_rep_soc: undefined,denomination_ste_rep_soc: undefined,nom_ste_rep_soc: undefined,residence_ste_rep_soc: undefined,batiment_ste_rep_soc: undefined,entree_ste_rep_soc: undefined,appart_ste_rep_soc: undefined,etage_ste_rep_soc: undefined,boite_postale_ste_rep_soc: undefined,code_postal_ste_rep_soc: undefined,ville_ste_rep_soc: undefined,pays_ste_rep_soc: undefined,civilite_ste_rep_soc_rep: undefined ,nom_ste_rep_soc_rep: undefined,prenom_ste_rep_soc_rep: undefined,ddn_ste_rep_soc_rep: undefined,vdn_ste_rep_soc_rep: undefined,telephone_ste_rep_soc_rep: undefined,email_ste_rep_soc_rep: undefined});
    this.setState({type_representant: choice.target.value});
  }
  handleClickChoiceTypeRepresentantAssocie = choice => {
    this.setState({  civilite_associe_rep_part: undefined,nom_associe_rep_part: undefined,prenom_associe_rep_part: undefined,ddn_associe_rep_part: undefined,vdn_associe_rep_part: undefined,telephone_associe_rep_part: undefined,email_associe_rep_part: undefined,residence_associe_rep_part: undefined,batiment_associe_rep_part: undefined,entree_associe_rep_part: undefined,appart_associe_rep_part: undefined,etage_associe_rep_part: undefined,code_postal_associe_rep_part: undefined,ville_associe_rep_part: undefined,pays_associe_rep_part: undefined,type_associe_rep_soc: undefined,denomination_associe_rep_soc: undefined,nomsoc_associe_rep_soc: undefined,residence_associe_rep_soc: undefined,batiment_associe_rep_soc: undefined,entree_associe_rep_soc: undefined,appart_associe_rep_soc: undefined,etage_associe_rep_soc: undefined,boite_postale_associe_rep_soc: undefined,code_postal_associe_rep_soc: undefined,ville_associe_rep_soc: undefined,pays_associe_rep_soc: undefined,civilite_associe_rep_soc: undefined,nom_associe_rep_soc: undefined,prenom_associe_rep_soc: undefined,ddn_associe_rep_soc: undefined,vdn_associe_rep_soc: undefined,telephone_associe_rep_soc: undefined,email_associe_rep_soc: undefined});
    this.setState({type_representant_associe_sup: choice.target.value});
  }

  handleClickDeuxProprio = event => {
    if (this.state.deuxProprio === undefined){
      this.setState({deuxProprio: true});
    }
    else if (this.state.deuxProprio === false) {
      this.setState({deuxProprio: true});
    }
    else if (this.state.deuxProprio === true) {
      this.setState({deuxProprio: false});
      this.setState({deux_civilite: undefined,deux_nom: undefined,deux_prenom: undefined,deux_email: undefined,deux_tel: undefined,deux_code_postal: undefined,deux_complement: undefined,deux_entree: undefined,deux_etage: undefined, deux_batiment: undefined,deux_numero: undefined,deux_pays: undefined,deux_residence: undefined,deux_rue: undefined,deux_ville: undefined, adresse_dif: false, deux_appart: undefined});
    }
    //this.setState({[element.name]: element.value});
  }

  handleClickDeuxIndiv = event => {
    if (this.state.deuxIndiv === undefined){
      this.setState({deuxIndiv: true});
    }
    else if (this.state.deuxIndiv === false) {
      this.setState({deuxIndiv: true});
    }
    else if (this.state.deuxIndiv === true) {
      this.setState({deuxIndiv: false});
      this.setState({deux_nom: undefined, deux_prenom: undefined, deux_residence: undefined, deux_batiment: undefined, deux_complement: undefined, deux_numero: undefined, deux_rue: undefined, deux_ddn: undefined,
      deux_vdn: undefined, deux_code_postal: undefined, deux_ville: undefined,deux_email: undefined, deux_pays: undefined, deux_telephone: undefined, deux_appart: undefined, deux_etage: undefined, deux_entree: undefined, adresse_dif: false});
    }
  }

  handleClickParticulierAdresseDif = event => {
    if (this.state.adresse_dif === undefined){
      this.setState({adresse_dif: true});
    }
    else if (this.state.adresse_dif === false) {
      this.setState({adresse_dif: true});
    }
    else if (this.state.adresse_dif === true) {
      this.setState({adresse_dif: false});
      this.setState({deux_code_postal: undefined,deux_complement: undefined,deux_infos_batiment: undefined,deux_numero: undefined,deux_pays: undefined,deux_residence: undefined,deux_rue: undefined,deux_ville: undefined, deux_telephone: undefined});
    }
  }

  handleClickIndivisaireAdresseDif = event => {
    if (this.state.adresse_dif === undefined){
      this.setState({adresse_dif: true});
    }
    else if (this.state.adresse_dif === false) {
      this.setState({adresse_dif: true});
    }
    else if (this.state.adresse_dif === true) {
      this.setState({adresse_dif: false});
      this.setState({deux_residence: undefined, deux_batiment: undefined, deux_complement: undefined, deux_numero: undefined, deux_rue: undefined, deux_code_postal: undefined, deux_ville: undefined, deux_pays: undefined, deux_telephone: undefined, deux_appart: undefined, deux_etage: undefined, deux_entree: undefined});
    }
  }

  handleClickDeuxAssocie = event => {
    if (this.state.deuxAssocie === undefined){
      this.setState({deuxAssocie: true});
    }
    else if (this.state.deuxAssocie === false) {
      this.setState({deuxAssocie: true});
    }
    else if (this.state.deuxAssocie === true) {
      this.setState({deuxAssocie: false});
      this.setState({type_representant_associe_sup: undefined,deux_nom: undefined, deux_prenom: undefined, deux_residence: undefined, deux_infos_batiment: undefined, deux_complement: undefined, deux_numero: undefined, deux_rue: undefined, deux_code_postal: undefined, deux_ville: undefined, deux_pays: undefined, deux_telephone: undefined, adresse_dif: false});
    }
  }

  handleClickTVA = event => {
    if (this.state.tva_ok === undefined){
      this.setState({tva_ok: true});
    }
    else if (this.state.tva_ok === false) {
      this.setState({tva_ok: true});
    }
    else if (this.state.tva_ok === true) {
      this.setState({tva_ok: false});
      this.setState({num_tva_societe: undefined, deux_nom: undefined, deux_prenom: undefined, deux_residence: undefined, deux_infos_batiment: undefined, deux_complement: undefined, deux_numero: undefined, deux_rue: undefined, deux_code_postal: undefined, deux_ville: undefined, deux_pays: undefined, deux_telephone: undefined, adresse_dif: false});
    }
  }


  handleChange = event => {
    this.setState({[event.currentTarget.name]: event.currentTarget.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    scroll.scrollToTop();
    const currentMaxId = this.props.id;

    console.log("hello");

    const id = Number(currentMaxId + 1);
    console.log(id);
    const form_object = this.state;

    this.props.onForm1Add({ id, form_object });
    this.setState({ });


  };


    render(){
      console.log("etat de Form1");
      console.log(this.state);
      var typePersonne = this.state.type_personne;
      var typeFormulaire;
      if (typePersonne === "particulier"){
        typeFormulaire = <FormParticulier onSubmit={this.handleSubmit} prevState={this.state} onClickDeuxProprio={this.handleClickDeuxProprio} onClickAdresseDif={this.handleClickParticulierAdresseDif} onChange={this.handleAdd}/>;
    }
      else if (typePersonne === "indivisaire") {
        typeFormulaire = <FormIndivision onSubmit={this.handleSubmit} prevState={this.state} onChange={this.handleAdd} onClickAdresseDif={this.handleClickIndivisaireAdresseDif} onClickDeuxIndiv={this.handleClickDeuxIndiv} />;
      }
      else if (typePersonne === "entreprise") {
        typeFormulaire = <FormSci onClickTVA={this.handleClickTVA} onClickTypeRepresentantAssocie={this.handleClickChoiceTypeRepresentantAssocie} onClickTypeRepresentant={this.handleClickChoiceTypeRepresentant} onSubmit={this.handleSubmit} onClickDeuxAssocie={this.handleClickDeuxAssocie} prevState={this.state} onChange={this.handleAdd} />;
      }
        return (

            <div className="display">
                <div className="container_blue">
                  <div className="container_form_form1 _4">
                    <p>Bienvenue à vous,</p>
                  </div>
                  <p className="padding_r_15p">Pour enregistrer votre 1er bien loué, vous aurez besoin des mêmes informations figurant au bail de location.
                                           Commençons tout d’abord par son propriétaire, conformément à l’Acte de Propriété :</p>
                </div>
                <div className="container_form_form1 _2">
                    <p>IDENTITÉ PROPRIÉTAIRE(S)</p>
                </div>
                <div className="container_row margin-t2vh">
                  <div className="container_input_local margin-b20">

                          <input type="radio"
                              id="choice1"
                              onChange={this.handleClickChoiceTypePersonne}
                              name="type_personne"
                              value="particulier">
                          </input>

                          <label className="label_blue" htmlFor="choice1">VOUS ÊTES UN PARTICULIER</label>
                  </div>
                  <div className="container_input_local margin-b20">

                          <input type="radio"
                              id="choice2"
                              onChange={this.handleClickChoiceTypePersonne}
                              name="type_personne"
                              value="indivisaire">
                          </input>

                        <label className="label_blue" htmlFor="choice2">VOUS ÊTES UN INDIVISAIRE DU BIEN EN LOCATION</label>
                  </div>
                  <div className="container_input_local margin-b20">

                          <input type="radio"
                              id="choice3"
                              onChange={this.handleClickChoiceTypePersonne}
                              name="type_personne"
                              value="entreprise">
                          </input>

                          <label className="label_blue" htmlFor="choice3">VOUS ÊTES UNE SCI, SCPI, SARL familiale, OU Autre</label>
                  </div>
                </div>
                        {typeFormulaire}
                </div>
        );
    }
}
export default Form1;
