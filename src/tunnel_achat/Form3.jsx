import React, { Component } from "react";
import { Link, DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

class Form3 extends Component {

  handleChange = event => {
    this.setState({[event.currentTarget.name]: event.currentTarget.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    scroll.scrollToTop();
    const currentMaxId = this.props.id;

    console.log("hello");

    const id = Number(currentMaxId + 1);
    console.log(id);
    const form_object = this.state;

    this.props.onForm1Add({ id, form_object });
    this.setState({ });

  };

  render() {
    const HT = 5;
    const TVA = 20;
    const result = HT * (1 + TVA / 100);
    console.log(this.state);
  return (
    <form onSubmit={this.handleSubmit} >
    <div className="display_1">
      <div className="container_form_form1 _2">
        <p className="paragraphe1">NATURE DE BIEN LOUÉ</p>
      </div>
        <div className="container_blue">
          <p>
            > Indiquez ici le nombre de pièces habitables: Studio, F2, F3, etc.
            <br />> Les pièces supérieures à 28m² s'identifient comme "Bis".
            <br />> Ne comptez pas les pièces annexes comme les buanderies, chaufferie,
          </p>
        </div>
        <div className="select_Form3">
          <label htmlFor="NatureLogement">Selectionner le type de bien : </label>
          <br/>
          <select onChange={this.handleChange} name="taille_bien" size="1">

            <option defaultValue="disabled selected">F1</option>
            <option value = "F1_bis">F1 bis</option>
            <option value = "F2">F2</option>
            <option value ="F2_bis">F2 bis</option>
            <option value = "F3">F3</option>
            <option value = "F3_bis">F3 bis</option>
            <option value = "F4">F4</option>
            <option value = "F4_bis">F4 bis</option>
            <option value = "F5">F5</option>
            <option value = "F5_bis">F5 bis</option>
            <option value = "F6">F6</option>
            <option value = "F6_bis">F6 bis</option>
            <option value = "F7">F7</option>
            <option value = "F7_bis">F7 bis</option>
            <option value = "F8">F8</option>
            <option value = "F8_bis">F8 bis</option>
            <option value = "F9">F9</option>
            <option value = "F9_bis">F9 bis</option>
            <option value = "F10">F10</option>
            <option value = "F10_bis">F10 bis</option>
            <option value = "F11">F11</option>
            <option value = "F11_bis">F11 bis</option>
            <option value = "F12">F12</option>
            <option value = "F12_bis">F12 bis</option>
          </select>
        </div>
          <div className="container_blue">
            <p>
                    > Pourquoi dit-on F2 ou T2 ?
              <br />> Qu'est ce qu'une piece principale ?
              <br />> Qu'est ce qu'un studio ?
              <br />> Qu'est ce qu'un F1, F2, F3 ?
            </p>
          </div>
          <div className="container_bolder margin-t10px margin-b20">
            <p>Votre bien en location est un :</p>
          </div>
          <div className="space_between container_button margin-b20 margin-t10px">
            <div className="margin-l150px">
              Appartement F4 :
            </div>
            <div className="blue_color">
            {result} € TTC
            </div>
          </div>
          <div className="space_between container_button margin-t10px">
            <div className="margin-l150px">
            Votre contrat annuel :
            </div>
            <div className="blue_color">
            {result} € TTC
            </div>
          </div>
          <div className="container_blue">
            <p>Déductible des impôts sur les revenus</p>
          </div>
          <div className="display_row">
            <div className="display_column">
              <div className="container_bolder">
                <p>Un code promotionnel ?</p>
              </div>
              <div className="container_small">
                (Grâce au parrainage de votre locataire)
              </div>
            </div>
            <div>
              <input onChange={this.handleChange} type="text" id="code_promo" name="code_promo" placeholder="Entrer votre code ici" minLength="4" maxLength="8" size="10"/>
            <div className="container_button displayflex_center">
              <button type="submit">CONTINUER</button>
            </div>
          </div>
        </div>
      </div>
      </form>
    );
  }
}
export default Form3;
