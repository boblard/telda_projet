import React, { Component } from "react";
import ReactDOM from "react-dom";

import HeaderPage from './HeaderPage';
import ClientForm from './ClientForm';

class Inscription extends Component {
  render(){
    return(
      <div>
        <HeaderPage />
        <ClientForm />
      </div>
    )
  }
}
export default Inscription;
