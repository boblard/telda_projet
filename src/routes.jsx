import React from 'react';
import { BrowserRouter as Route } from 'react-router-dom';

import HomePage from './homepage/HomePage';

export default function MainRouter () {
    return (
        <Router>
            <div>
              <BrowserRouter>
                <Route exact path="/" component={HomePage}/>
              </BrowserRouter>
            </div>
        </Router>
    )
}
