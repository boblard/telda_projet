import React , { Component } from "react";

export default class TestPostApi extends Component {

  constructor(props) {
		super(props);
		this.state = {param: []};
	}

	componentDidMount() {
		fetch('http://137.74.162.201:8081/telda3/telda3/parametrage', {
			method: 'POST',
			body: JSON.stringify({
        "adresse": "adresse",
        "assurance_souscrite": "assurance",
        "capital_social": "capital",
        "email": "email",
        "email_assureur": "email de l'assureur",
        "forme_juridique": "forme",
        "nom_prestation": "prestation",
        "numero_siren": 7857960,
        "numero_siret": 97689708,
        "penalite_retard": "penalité",
        "raison_sociale": "raison",
        "telephone_assureur": "0789878998",
        "telephone_telda": "678338763",
        "type_tva": "tva",
        "ville_immatriculation": "villeimmat"
			}),
			headers: {
				"Content-type": "application/json;charset=UTF-8"
			}
		}).then(response => {
				return response.json()
			}).then(json => {
				this.setState({
					param:json
				});
			});
	}
	render() {
		return (
			<div>
				<p><b>New Resource created in the server as shown below</b></p>
				<p>id : {this.state.param.id}</p>
        <p>adresse : {this.state.param.adresse}</p>
				<p>assurance_souscrite : {this.state.param.assurance_souscrite}</p>
				<p>capital_social : {this.state.param.capital_social}</p>
				<p>email : {this.state.param.email}</p>
        <p>email_assureur : {this.state.param.email_assureur}</p>
  			<p>forme_juridique : {this.state.param.forme_juridique}</p>
  			<p>nom_prestation : {this.state.param.nom_prestation}</p>
  			<p>numero_siren : {this.state.param.numero_siren}</p>
        <p>numero_siret : {this.state.param.numero_siret}</p>
    		<p>penalite_retard : {this.state.param.penalite_retard}</p>
    		<p>raison_sociale : {this.state.param.raison_sociale}</p>
    		<p>telephone_assureur : {this.state.param.telephone_assureur}</p>
        <p>telephone_telda : {this.state.param.telephone_telda}</p>
      	<p>type_tva : {this.state.param.type_tva}</p>
      	<p>ville_immatriculation : {this.state.param.ville_immatriculation}</p>
			</div>
		)
	}
}
